
#include <unistd.h> // for sleep
#include <thread>

#include <cv.h>
#include "highgui.h"                                                                                   

#include "log.h"
#include "Vect.h"

#include "StaticBodyModel.h"
#include "BodyView.h"

//#include "OSG/ScreenDriverOSG.h"
#include "SDL/ScreenDriverSDL.h"


using namespace std;
using namespace metric;

BodyView* createPattern( ScreenDriver *driver, Vect2f vMin, Vect2f vDim )
{
	StaticBodyModel *body = new StaticBodyModel();

	Vect2f vCenter = vMin + vDim / 2.0;
	body->setPosition( vCenter );
	body->setExtents( vDim / 2.0 );
	body->setShape( BodyModel::BODY_SHAPE_BOX );

	BodyView *view = driver->makeBody( body, "wall" );
	view->setDrawingMode(BodyView::DRAW_SOLID);
	view->setColor(Color(255,255,255));

	return view;
}


std::vector<BodyView*> createPatterns( ScreenDriver *driver )
{
	Vect2f vDim( driver->screen()->world().size() );
	Vect2f vHalf( vDim / 2 );
	std::vector<BodyView*> views;

	views.push_back( createPattern(driver, Vect2f(0,0), vDim) );
	views.push_back( createPattern(driver, Vect2f(0,0), vHalf) );
	views.push_back( createPattern(driver, Vect2f(vHalf.x(),0), vHalf) );
	views.push_back( createPattern(driver, Vect2f(0,vHalf.y()), vHalf) );
	views.push_back( createPattern(driver, vHalf, vHalf) );

	return views;
}

int main(int argc, char *argv[])
{
	/*
	IplImage *frame = 0;
	IplImage *gray = 0;
	CvCapture* capture = cvCreateCameraCapture(-1);
	if (!capture) {
		M_ERROR << "Failed to open camera." << endl;
		return 1;
	}
	cvSetCaptureProperty( capture, CV_CAP_PROP_FRAME_WIDTH, 320);
	cvSetCaptureProperty( capture, CV_CAP_PROP_FRAME_HEIGHT, 240);
	cvSetCaptureProperty( capture, CV_CAP_PROP_FPS, 125);
    CvMemStorage* storage = cvCreateMemStorage(0);

	frame = cvQueryFrame(capture);
	if (!frame) {
		M_ERROR << "Failed to capture image" << endl;
		return 1;
	}

	if (!gray) {
		gray = cvCreateImage(cvGetSize(frame), 8, 1);
		if (!gray) {
			M_ERROR << "Failed to convert to greyscale" << endl;
			return 1;
		}
	}
	*/

	ScreenDriver *driver = new ScreenDriverSDL(argc, argv);
	if (!driver) {
		M_ERROR << "Failed to open screen driver." << endl;
		return 1;
	}

	Screen *screen = driver->screen();
	if (!screen) {
		M_ERROR << "Failed to open screen." << endl;
		delete driver;
		return 1;
	}
	//screen->setBgColor(Color(0,0,0));
	screen->open("Screen Calibration");

	std::vector<BodyView*> views = createPatterns( driver );

	/*
	views[0]->setColor(Color(255,0,0));
	views[1]->setColor(Color(0,255,0));
	views[2]->setColor(Color(0,0,255));
	views[3]->setColor(Color(255,255,0));
	views[4]->setColor(Color(0,255,255));
	*/

	for (auto view: views) {
		view->setVisible( false );
		//view->setColor(Color(0,0,255));
	}

	// Spawn thread to execute the camera detection
	std::thread cameraThread([=]() { 
		//IplImage *img;
		views[0]->setVisible(true);
		for (int i = 0; i < views.size()-1; i++) {
			sleep(1);
			//img = cvQueryFrame(capture);
			views[i]->setVisible(false);
			views[i+1]->setVisible(true);
		}
		sleep(1);
		//img = cvQueryFrame(capture);
		delete driver;
		exit(0);
	});

	// Use current thread to execute the screen driver. 
	// Qt will refuse to run in anything but the main thread.
	driver->exec();

	// Wait for physics engine to close:
	//engine->quit();
	cameraThread.join();

	delete screen;
	delete driver;
	return 0;
}
