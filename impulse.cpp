
class Impulse
{	
public:
	Impulse() {
		_duration = 0.0;
	}
	Impulse( Vect2d pos, Vect2d dir ) {
		_pos = pos;
		_dir = dir;
		_duration = 0.0;
	}
	virtual ~Impulse() {
		// erase self from screen
		if( _bounds.w != 0 && _bounds.h != 0) {
//			BLIT(g_table, &_bounds, g_screen, &_bounds);
		}
	}

	static bool expired( const Impulse &imp ) {
		return imp._duration > 2.0;
	}

	Vect2d _pos;
	Vect2d _dir;
	double _duration;
	SDL_Rect _bounds;
};

std::list<Impulse> g_impulses;



void draw_impulse( Impulse &imp )
{
	// [m ] Collision: [-0.474126,102.415] [0,0]
	// [m ] draw_impulse: p1:[0.498881,0.954137] p2:[0.496107,1.08842]
	// [m ] draw_impulse: b4 bounds:(833,-91) -> (5,139)
	// [m ] draw_impulse: s1:(2512,48) s2:(2512,0)
	// [m ] draw_impulse: aft bounds:(2512,0) -> (64704,90)
	// [m ] OOB at: draw_impulse:309 "update_rects"  (1680,64,0,0)
	// [m ] OOB at: draw_impulse:310 "update_rects"  (2512,0,64704,90)
	//
	Vect2d p1( imp._pos );
	Vect2d p2( p1 + imp._dir );

	// erase old body position by blitting table image over it
	if( imp._bounds.w != 0 && imp._bounds.h != 0 ) {
		BLIT(g_table, &imp._bounds, g_screen, &imp._bounds);
	}
	SDL_Rect prevBounds = imp._bounds;

	Vect2d table( g_tableWidth, g_tableLength );
	USER << "draw_impulse: p1:" << p1/table << " p2:" << p2/table << endl;

	// screen coordinates of p1 and p2
	Sint16 s1[2], s2[2];
	world_to_screen( p1.x, p1.y, s1[0], s1[1] );
	world_to_screen( p2.x, p2.y, s2[0], s2[1] );
	oob_adjust( s1 );
	oob_adjust( s2 );

	SDL_Rect bounds;
	bounds.x = std::min( s1[0], s2[0] );
	bounds.y = std::min( s1[1], s2[1] );
	bounds.w = std::max( s1[0], s2[0] ) - bounds.x;
	bounds.h = std::max( s1[1], s2[1] ) - bounds.y;

	USER << "draw_impulse: b4 s1:(" << s1[0] << "," << s1[1] << ")";
	USER << " s2:(" << s2[0] << "," << s2[1] << ")" << endl;

	USER << "draw_impulse: b4 bounds:(" << bounds.x << "," << bounds.y << ") -> ";
	USER << "(" << bounds.w << "," << bounds.h << ")" << endl;

	oob_adjust( &bounds );

	// update a bit more than the actual bounds
	Sint16 radius = 2;
	if((int)bounds.x + bounds.w + radius < SCREEN_WIDTH)
		bounds.w += radius;
	else
		bounds.w = SCREEN_WIDTH-1 - bounds.x;
	if((int)bounds.y + bounds.h + radius < SCREEN_HEIGHT)
		bounds.h += radius;
	else
		bounds.h = SCREEN_HEIGHT-1 - bounds.y;
	if(bounds.x >= radius) bounds.x -= radius; else bounds.x = 0;
	if(bounds.y >= radius) bounds.y -= radius; else bounds.y = 0;

	USER << "draw_impulse: mid s1:(" << s1[0] << "," << s1[1] << ")";
	USER << " s2:(" << s2[0] << "," << s2[1] << ")" << endl;

	USER << "draw_impulse: mid bounds:(" << bounds.x << "," << bounds.y << ") -> ";
	USER << "(" << bounds.w << "," << bounds.h << ")" << endl;

	oob_adjust( s1, &bounds );
	oob_adjust( s2, &bounds );

	// [m ] Collision: [-12.1521,42.451] [0,0]
	// [m ] draw_impulse: p1:[0.497316,0.953996] p2:[0.4945,1.01062]
	// [m ] draw_impulse: s1:(835,1105) s2:(830,65525)
	// [m ] draw_impulse: bounds:(826,1105) -> (9,65481)

	USER << "draw_impulse: aft s1:(" << s1[0] << "," << s1[1] << ")";
	USER << " s2:(" << s2[0] << "," << s2[1] << ")" << endl;

	USER << "draw_impulse: aft bounds:(" << bounds.x << "," << bounds.y << ") -> ";
	USER << "(" << bounds.w << "," << bounds.h << ")" << endl;

	Draw_Line(g_screen, s1[0], s1[1], s2[0], s2[1],
			  SDL_MapRGB(g_screen->format, 255, 255, 255));

	UPDATE_RECTS(g_screen, 1, &prevBounds); // update erased 
	UPDATE_RECTS(g_screen, 1, &bounds); // update newly drawn
}

BodyModel* create_last_collision()
{
	double radius = 2.0;
	BodyModel *body = new BodyModel(false); // not simulated
	body->setRadius( radius );

	Vect2d pos( g_tableWidth / 2, g_tableLength / 4 );
	body->setPosition(pos);

	BodyView *view = new BodyView();
	// convert mallet radius into width and height in screen pixels
	Uint16 w, h;
	world_to_screen(radius, radius, w, h);
	view->radiusScreen = w;
	USER << "collision screen radius: " << w << endl;

	view->get_screen_rect(pos, view->bounds);
	body->view( view );
	return body;
}

void step()
{
	/////////////
	// Impulse rendering
	////
	std::list<Impulse>::iterator iPulse = g_impulses.begin();
	for ( ; iPulse != g_impulses.end(); ++iPulse )
	{
		Impulse &imp = *iPulse;
		imp._duration += g_current_dtime;
		draw_impulse( imp );
	}
	g_impulses.remove_if( Impulse::expired );
}



