# - Try to find ODE
# Once done, this will define
#
#  ODE_FOUND - system has ODE
#  ODE_INCLUDE_DIR - the ODE include directories
#  ODE_LIBRARY - link these to use ODE 

include(LibFindMacros)

# Use pkg-config to get hints about paths
libfind_pkg_check_modules(ODE_PKGCONF ode)

# Main include dir
find_path(ODE_INCLUDE_DIR
  NAMES ode/ode.h
  PATHS ${ODE_PKGCONF_INCLUDE_DIRS}
  PATH_SUFFIXES ode
)

# Finally the library itself
find_library(ODE_LIBRARY
  NAMES ode
  PATHS ${ODE_PKGCONF_LIBRARY_DIRS}
)

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
set(ODE_PROCESS_INCLUDES ODE_INCLUDE_DIR)
set(ODE_PROCESS_LIBS ODE_LIBRARY)
libfind_process(ODE)
