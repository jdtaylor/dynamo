# - Try to find ODE
# Once done, this will define
#
#  OPENSCENEGRAPH_FOUND - system has ODE
#  OPENSCENEGRAPH_INCLUDE_DIR - the ODE include directories
#  OPENSCENEGRAPH_LIBRARY - link these to use ODE 

include(LibFindMacros)

# Use pkg-config to get hints about paths
libfind_pkg_check_modules(OPENSCENEGRAPH_PKGCONF openscenegraph)

# Includes for Coin3D
find_path(OPENSCENEGRAPH_INCLUDE_DIR
  NAMES osg/CameraNode
  PATHS ${OPENSCENEGRAPH_PKGCONF_INCLUDE_DIRS}
  PATH_SUFFIXES osg
)

# Finally the library itself
find_library(OSG_LIBRARY
  NAMES osg
  PATHS ${OPENSCENEGRAPH_PKGCONF_LIBRARY_DIRS}
)

find_library(OSG_UTIL_LIBRARY
  NAMES osgUtil
  PATHS ${OPENSCENEGRAPH_PKGCONF_LIBRARY_DIRS}
)

find_library(OSG_TEXT_LIBRARY
  NAMES osgText
  PATHS ${OPENSCENEGRAPH_PKGCONF_LIBRARY_DIRS}
)

find_library(OSG_GA_LIBRARY
  NAMES osgGA
  PATHS ${OPENSCENEGRAPH_PKGCONF_LIBRARY_DIRS}
)

find_library(OSG_DB_LIBRARY
  NAMES osgDB
  PATHS ${OPENSCENEGRAPH_PKGCONF_LIBRARY_DIRS}
)

find_library(OSG_VIEWER_LIBRARY
  NAMES osgViewer
  PATHS ${OPENSCENEGRAPH_PKGCONF_LIBRARY_DIRS}
)

set(OSG_LIBRARIES ${OSG_LIBRARY}
	${OSG_UTIL_LIBRARY}
	${OSG_TEXT_LIBRARY}
	${OSG_GA_LIBRARY}
	${OSG_DB_LIBRARY}
	${OSG_VIEWER_LIBRARY})

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
set(OPENSCENEGRAPH_PROCESS_INCLUDES OPENSCENEGRAPH_INCLUDE_DIR)
set(OPENSCENEGRAPH_PROCESS_LIBS OPENSCENEGRAPH_LIBRARY)
libfind_process(OpenSceneGraph)

