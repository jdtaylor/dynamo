# - Try to find ODE
# Once done, this will define
#
#  OPENAL_FOUND - system has ODE
#  OPENAL_INCLUDE_DIR - the ODE include directories
#  OPENAL_LIBRARY - link these to use ODE 

include(LibFindMacros)

# Use pkg-config to get hints about paths
libfind_pkg_check_modules(OPENAL_PKGCONF OpenAL)

# Main include dir
find_path(OPENAL_INCLUDE_DIR
  NAMES AL/al.h
  PATHS ${OPENAL_PKGCONF_INCLUDE_DIRS}
  PATH_SUFFIXES AL
)

# Finally the library itself
find_library(OPENAL_LIBRARY
  NAMES openal
  PATHS ${OPENAL_PKGCONF_LIBRARY_DIRS}
)

find_library(ALUT_LIBRARY
  NAMES alut
  PATHS ${OPENAL_PKGCONF_LIBRARY_DIRS}
)

set(OPENAL_LIBS ${OPENAL_LIBRARY} ${ALUT_LIBRARY})

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
set(OPENAL_PROCESS_INCLUDES OPENAL_INCLUDE_DIR)
set(OPENAL_PROCESS_LIBS OPENAL_LIBS)
libfind_process(OpenAL)

