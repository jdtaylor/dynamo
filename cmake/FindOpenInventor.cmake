# - Try to find ODE
# Once done, this will define
#
#  OPENINVENTOR_FOUND - system has ODE
#  OPENINVENTOR_INCLUDE_DIR - the ODE include directories
#  OPENINVENTOR_LIBRARY - link these to use ODE 

include(LibFindMacros)

# Use pkg-config to get hints about paths
libfind_pkg_check_modules(OPENINVENTOR_PKGCONF Coin)

# Main include dir OpenInventor
#find_path(OPENINVENTOR_INCLUDE_DIR
#  NAMES Inventor/So.h
#  PATHS ${OPENINVENTOR_PKGCONF_INCLUDE_DIRS}
#  PATH_SUFFIXES Inventor
#)

# Includes for Coin3D
find_path(OPENINVENTOR_INCLUDE_DIR
  NAMES Inventor/So.h
  PATHS ${OPENINVENTOR_PKGCONF_INCLUDE_DIRS}
  PATH_SUFFIXES coin
)

# Finally the library itself
#find_library(OPENINVENTOR_LIBRARY
#  NAMES Inventor
#  PATHS ${OPENINVENTOR_PKGCONF_LIBRARY_DIRS}
#)
find_library(COIN_LIBRARY
  NAMES Coin
  PATHS ${OPENINVENTOR_PKGCONF_LIBRARY_DIRS}
)

find_library(SO_QT_LIBRARY
  NAMES SoQt
  PATHS ${OPENINVENTOR_PKGCONF_LIBRARY_DIRS}
)

# I'm not sure what I'm doing here, but it seems to work:
set(OPENINVENTOR_LIBRARY ${COIN_LIBRARY} ${SO_QT_LIBRARY})

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
set(OPENINVENTOR_PROCESS_INCLUDES OPENINVENTOR_INCLUDE_DIR)
set(OPENINVENTOR_PROCESS_LIBS OPENINVENTOR_LIBRARY)
libfind_process(OpenInventor)
