
#ifndef __SCREEN_FACTORY_OSG_H
#define __SCREEN_FACTORY_OSG_H

#include <map>

//#include "SDL.h"

#include <osgGA/GUIEventHandler>

#include "ScreenDriver.h"
#include "ScreenOSG.h"

namespace metric {

class ScreenDriverOSG : public ScreenDriver
{
public:
	ScreenDriverOSG(int argc, char *argv[]);
	virtual ~ScreenDriverOSG();

	static ScreenDriverOSG& instance();

	virtual void exec();

	virtual bool loadResources();

	virtual ScreenOSG* screen() { return dynamic_cast<ScreenOSG*>( _screen ); }

	virtual BodyView* makeBody(BodyModel *body, const std::string &resource);

	bool handleGuiEvent(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter &aa);

protected:
	osg::Camera* createHUD();

	//SDL_Surface* getResource(const std::string &resource);
	//typedef std::map<std::string, SDL_Surface*> ResourceMap;
	//ResourceMap _resources;

	static ScreenDriverOSG *g_instance;
};

}

#endif

