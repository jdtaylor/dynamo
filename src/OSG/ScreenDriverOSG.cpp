
#include <osg/ArgumentParser>
#include <osgViewer/Viewer>
#include <osgDB/ReadFile>
#include <osg/Timer>

#include "log.h"
#include "const.h"
#include "ScreenDriverOSG.h"
#include "ScreenOSG.h"
#include "BodyViewOSG.h"

using namespace metric;
using namespace std;

// construct the viewer.
osgViewer::Viewer* g_viewer = NULL;
ScreenDriverOSG* ScreenDriverOSG::g_instance = NULL;

ScreenDriverOSG& ScreenDriverOSG::instance()
{
	if (!g_instance) {
		M_ERROR << "Instantiate ScreenDriver Immediately!" << endl;
		g_instance = new ScreenDriverOSG(0, NULL);
	}
	return *g_instance;
}

class myKeyboardEventHandler : public osgGA::GUIEventHandler
{
public:
	virtual bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter &aa)
	{
		return ScreenDriverOSG::instance().handleGuiEvent(ea, aa);
	}

	/*
	virtual void accept(osgGA::GUIEventHandlerVisitor& v)
	{
		v.visit(*this);
	}
	*/
};


bool ScreenDriverOSG::handleGuiEvent(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter &aa)
{
	switch(ea.getEventType())
	{
		case(osgGA::GUIEventAdapter::KEYDOWN):
		{
			switch(ea.getKey())
			{
			case 'q':
				g_viewer->setDone(true); // exits viewer loop
				break;
			} 
			break;
		}
		case(osgGA::GUIEventAdapter::MOVE):
		{
			{
				static osg::Timer_t t1 = 0.0;
				static osg::Timer_t t2 = 0.0;
				static double fps = 0.0;

				t2 = osg::Timer::instance()->tick();
				fps = fps * 0.99 + osg::Timer::instance()->delta_s(t1, t2) * 0.01;
				//M_USER << "FPS: " << 1.0 / fps << endl;
				t1 = t2;
			}

			Vect2f pos( ea.getX(), ea.getY() );

			// Correct for mallet collisions with the wall
			BodyViewOSG *controlledView = dynamic_cast<BodyViewOSG*>( _controlledBodyView );

//			if (controlledView) {
//			   controlledView->correct_pos( pos );
//			}

			// Convert mouse pos to world coordinates. put coord in 'worldPos'
			Vect2f worldPos;
			screen()->screen_to_world(worldPos, pos);

			//M_USER << "Move: " << pos << " -> " << worldPos << endl;

			// Emit mouse motion signal in world coordinates.  Screen coordinates 
			// should be kept within the Screen Driver.
			mouseMoved().emit( worldPos );

			//aa.requestRedraw();
			break;
		}
	}

	return false;
}

ScreenDriverOSG::ScreenDriverOSG(int argc, char *argv[]) :
	ScreenDriver(argc, argv)
{
	g_instance = this;

	// use an ArgumentParser object to manage the program arguments.
	osg::ArgumentParser arguments(&argc, argv);

	g_viewer = new osgViewer::Viewer( arguments );

	/*
	// read the scene from the list of file specified commandline args.
	osg::ref_ptr<osg::Node> scene = osgDB::readNodeFiles(arguments);

	if (!scene) {
		osgDB::readNodeFile("dynamo.osgt");
	}
	
	if (!scene) {
		M_ERROR << "Failed to init OSG" << endl;
	}
	*/

	_screen = new ScreenOSG( argc, argv );
}

ScreenDriverOSG::~ScreenDriverOSG()
{
	delete _screen;
}

void ScreenDriverOSG::exec()
{
	//g_viewer->setUpViewAcrossAllScreens();
	g_viewer->setUpViewInWindow(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

	//osgViewer::Viewer::Windows windows;
	//g_viewer->getWindows(windows);
	//if (windows.empty()) {
//		M_ERROR << "Failed to find any windows" << endl;
//		return;
	//}

	// set the scene to render
	g_viewer->setSceneData( screen()->getRoot() );

	g_viewer->addEventHandler(new myKeyboardEventHandler());

	// jdt 2013-06-04: This seems to help with event handler performance:
	g_viewer->setThreadingModel( osgViewer::ViewerBase::SingleThreaded );
	osgViewer::ViewerBase::ThreadingModel tm = g_viewer->getThreadingModel();
	string threadModel;
	switch(tm) {
		default: threadModel = "Unknown"; break;
		case osgViewer::ViewerBase::SingleThreaded:
			threadModel = "SingleThreaded";
			break;
		case osgViewer::ViewerBase::CullDrawThreadPerContext:
			threadModel = "ThreadPerContext";
			break;
		case osgViewer::ViewerBase::CullThreadPerCameraDrawThreadPerContext:
			threadModel = "CullThreadPerCameraDrawThreadPerContext";
			break;
		case osgViewer::ViewerBase::AutomaticSelection:
			threadModel = "AutomaticSelection";
			break;
	}
	M_USER << "Threading Model: " << threadModel << endl;

	//g_viewer->setRunMaxFrameRate(60.0);
	//g_viewer->setRunFrameScheme(osgViewer::Viewer::CONTINUOUS);
	//g_viewer->setRunFrameScheme(osgViewer::Viewer::ON_DEMAND);

	double fps = 0.0;
	int iFrame = 0;

	while (!g_viewer->done()) {
		osg::Timer_t t1 = osg::Timer::instance()->tick();

		g_viewer->frame();

		osg::Timer_t t2 = osg::Timer::instance()->tick();
		fps += osg::Timer::instance()->delta_s(t1, t2);
		if (iFrame % 100 == 0) {
			//M_USER << "FPS: " << (100.0 / fps) << endl;
			iFrame = -1;
			fps = 0.0;
		}

		iFrame++;
	}
	//g_viewer->run();
}

bool ScreenDriverOSG::loadResources()
{
	return true;
}

BodyView* ScreenDriverOSG::makeBody(BodyModel *body, const std::string &resource)
{
	BodyViewOSG *view = new BodyViewOSG( body, _screen );

	/*
	SDL_Surface *image = getResource( resource );
	if (image) {
		view->setSurface( image );
	}
	*/

	_bodyViews.push_back( view );

	return view;
}

/*
SDL_Surface* ScreenDriverOSG::getResource(const std::string &resource)
{
	ResourceMap::iterator it = _resources.find( resource );
	if(it == _resources.end()) {
		return NULL;
	}
	return it->second;
}
*/


