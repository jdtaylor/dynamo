
#include "log.h"

#include "ScreenOSG.h"

#include <osg/Node>
#include <osg/Group>
#include <osg/Geode>

#include <osgText/Text>

using namespace std;
using namespace metric;

ScreenOSG::ScreenOSG( int argc, char *argv[] ) :
	Screen( argc, argv ),
	_root(new osg::Group),
	_scene(new osg::Group),
	_camera(0)
{
	_camera = createCamera();
	_root->addChild( _camera );

	_camera->addChild( _scene );
}

osg::Camera * ScreenOSG::createCamera()
{
	osg::Camera *camera = new osg::Camera;

    // set the projection matrix
    //camera->setProjectionMatrix(osg::Matrix::ortho2D(0,2280,0,2024));
    camera->setProjectionMatrix(osg::Matrix::ortho2D(0, world().w(), 0, world().h()));

    // set the view matrix    
    camera->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
    camera->setViewMatrix(osg::Matrix::identity());

    // only clear the depth buffer
    camera->setClearMask(GL_DEPTH_BUFFER_BIT);

    // draw subgraph after main camera view.
    camera->setRenderOrder(osg::Camera::POST_RENDER);

    // we don't want the camera to grab event focus from the viewers main camera(s).
    camera->setAllowEventFocus(false);
    
	// This is to avoid swamping OSG_NOTIFY_LEVEL=INFO output with:
	// "_clampProjectionMatrix not applied, invalid depth range, znear=..."
	camera->setComputeNearFarMode(osg::Camera::DO_NOT_COMPUTE_NEAR_FAR);
	camera->setCullingMode(osg::Camera::NO_CULLING);


	return camera;
}

bool ScreenOSG::open( const std::string &windowTitle )
{
	return true;
}

bool ScreenOSG::close()
{
	return true;
}

unsigned int ScreenOSG::width() const
{
	return SCREEN_WIDTH;
}

unsigned int ScreenOSG::height() const
{
	return SCREEN_HEIGHT;
}

Rect2f ScreenOSG::world() const
{
	return Rect2f( Vect2f(0.0, 0.0), Vect2f(TABLE_WIDTH, TABLE_LENGTH) );
}

bool ScreenOSG::world_to_screen(const Vect2f &worldPos, Vect2f &screenPos)
{
	screenPos = Vect2f( worldPos.x() / world().dx() * width(),
					    worldPos.y() / world().dy() * height() );
	return true;
}

bool ScreenOSG::screen_to_world(Vect2f &worldPos, const Vect2f &screenPos)
{
	worldPos = Vect2f( screenPos.x() / width() * world().dx(),
					   screenPos.y() / height() * world().dy() );
	return true;
}

