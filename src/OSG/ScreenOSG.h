
#ifndef __METRIC_SCREEN_OSG_H
#define __METRIC_SCREEN_OSG_H 1

#include <vector>
#include <string>

#include <SDL/SDL.h>

#include <osg/CameraNode>
#include <osgUtil/SceneView>

#include "const.h"
#include "Screen.h"

namespace metric {


class ScreenOSG : public Screen
{
public:
	ScreenOSG( int argc, char *argv[] );
	virtual ~ScreenOSG() {}

	virtual bool open( const std::string &windowTitle );
	virtual bool close();

	virtual void setBgColor(char r, char g, char b) {}

	virtual unsigned int width() const;
	virtual unsigned int height() const;
	virtual Rect2f world() const;

	bool world_to_screen(const Vect2f &worldPos, Vect2f &screenPos);
	bool screen_to_world(Vect2f &worldPos, const Vect2f &screenPos);

	osg::Group* getRoot() { return _root; }
	osg::Group* getScene() { return _scene; }

	void addChild( osg::Node *child ) {
		if (_scene) {
			_scene->addChild( child );
		}
	}

protected:
	osg::ref_ptr<osgUtil::SceneView> _sceneView;
	osg::ref_ptr<osg::CameraNode> _cameraNode;
	osg::Group *_root;
	osg::Group *_scene;
	osg::Camera *_camera;

	osg::Camera* createCamera();
	//osg::Geode* createTests();
};

}

#endif

