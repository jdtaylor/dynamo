
#include <cassert>

#include <osg/ShapeDrawable>
#include <osg/Group>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/PositionAttitudeTransform>
#include <osg/Texture2D>

#include <osgDB/ReadFile>

#include "log.h"
#include "Vect.h"
#include "BodyViewOSG.h"


using namespace std;
using namespace metric;

BodyViewOSG::BodyViewOSG( BodyModel *bdy,
						  Screen *scr ) :
	BodyView(bdy, scr),
	_root( new osg::Group() ),
	_xform( new osg::PositionAttitudeTransform() ),
	_geode(0),
	_geometry(0)
{
	switch(body()->getShape())
	{
	default:
	case BodyModel::BODY_SHAPE_CYLINDER: {
		_geode = createAsCylinder();

		Vect2f pos( body()->getPosition() );
		_xform->setPosition( osg::Vec3(pos[0], pos[1], 0) );
		double radius = body()->getRadius();
		_xform->setScale( osg::Vec3(radius, radius, radius) );
		break;
	}
	case BodyModel::BODY_SHAPE_BOX:
		_geode = createAsBox();
		break;
	}

	_root->addChild( _xform );
	_xform->addChild( _geode );


	// Add our root to the scene 
	screen()->addChild( _root );
}

BodyViewOSG::~BodyViewOSG()
{
}

void BodyViewOSG::draw()
{
}

void BodyViewOSG::setVisible(bool visible)
{
	_geode->setNodeMask(visible ? ~0 : 0);
}

osg::Geode* BodyViewOSG::createAsCylinder()
{
	/*
	switch(getDrawingMode())
	{
		case BodyView::DRAW_WIREFRAME:
			SDL_Rect center;
			center.x = _bounds.x + _extentsScreen.x();
			center.y = _bounds.y + _extentsScreen.y();
			//printf("%d %d %d\n", m_rect.x, m_rect.y, m_rect.w);
			Draw_Circle(scr->surface(), center.x, center.y, _extentsScreen.x(),
					SDL_MapRGB(scr->surface()->format, 255, 255, 255));

			SDL_Rect angle; // I negate angle because SDL is upside down compared to world
			angle.x = (Sint16)(_extentsScreen.x() * cos( -body()->getAngle() ));
			angle.y = (Sint16)(_extentsScreen.y() * sin( -body()->getAngle() ));
			if(angle.x >= 1) angle.x--; 
			if(angle.y >= 1) angle.y--;
			//	circleColor(_screen, m_rect.x, m_rect.y, m_rect.w, 0xFFFFFFFF);
			Draw_Line(scr->surface(), center.x, center.y, center.x + angle.x,
					center.y + angle.y, SDL_MapRGB(scr->surface()->format, 255, 255, 255));
			//				255, 255, 255, 255);
			break;
		case BodyView::DRAW_SPRITE:
			// draw current position of body
			BLIT(_surface, NULL, scr->surface(), &_bounds);
			break;
	}
	*/
	osg::Geode *geode = new osg::Geode();
	_geometry = new osg::Geometry();

	geode->addDrawable( _geometry );

	Vect2f size( body()->getExtents() * 2 );
	Rect2f extents( body()->getPosition() - body()->getExtents(), size );

	osg::Vec3Array* vertices = new osg::Vec3Array;
	osg::Vec2Array* texcoords = new osg::Vec2Array;

	// triangle fan 
	osg::DrawElementsUInt* indices = new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLE_FAN, 0);
	unsigned int nVert = 64;
	for (unsigned int i = 0; i < nVert; ++i) {
		double rad = 2.0 * M_PI * ((double)i / nVert);
	    vertices->push_back( osg::Vec3(cos(rad), sin(rad), 0) );
		texcoords->push_back( osg::Vec2(cos(rad) / 2 + 0.5, sin(rad)/2 + 0.5) );
		indices->push_back(i);
	}

	_geometry->setVertexArray( vertices );
	_geometry->addPrimitiveSet( indices );
	_geometry->setTexCoordArray( 0, texcoords );

	osg::Texture2D *texture = new osg::Texture2D;
	texture->setDataVariance(osg::Object::DYNAMIC);
	osg::Image *image = osgDB::readImageFile("../resources/pucktest1.tga");
	if (!image) {
		M_ERROR << "Failed to load resource '../resources/pucktest1.tga'" << endl;
		return geode;
	}
	texture->setImage(image);

	osg::StateSet *stateOne = new osg::StateSet();
	// Assign texture unit 0 of new StateSet to the texture
	stateOne->setTextureAttributeAndModes(0, texture, osg::StateAttribute::ON);
	geode->setStateSet(stateOne);

	return geode;
}

osg::Geode* BodyViewOSG::createAsBox()
{
	osg::Geode *geode = new osg::Geode();
	_geometry = new osg::Geometry();

	geode->addDrawable( _geometry );

	Vect2f size( body()->getExtents() * 2 );
	Rect2f extents( body()->getPosition() - body()->getExtents(), size );

	osg::Vec3Array* vertices = new osg::Vec3Array;
	vertices->push_back( osg::Vec3( extents.x(), extents.y(), 0) ); // front left
	vertices->push_back( osg::Vec3( extents.x() + extents.w(), extents.y(), 0) ); // front right
	vertices->push_back( osg::Vec3( extents.x() + extents.w(),
									extents.y() + extents.h(), 0) ); // back right 
	vertices->push_back( osg::Vec3( extents.x(), extents.y() + extents.h(), 0) ); // back left 

	osg::DrawElementsUInt* boxQuad = new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS, 0);
	boxQuad->push_back(3);
	boxQuad->push_back(2);
	boxQuad->push_back(1);
	boxQuad->push_back(0);

	_geometry->setVertexArray( vertices );
	_geometry->addPrimitiveSet( boxQuad );

	/*
	ScreenOSG *scr = screen();
	if(!scr) return;

	switch(getDrawingMode())
	{
		case BodyView::DRAW_WIREFRAME:
			Draw_Rect(scr->surface(), _bounds.x, _bounds.y, _bounds.w, _bounds.h,
					SDL_MapRGB(scr->surface()->format, 255, 255, 255));
			break;
		case BodyView::DRAW_SPRITE:
			// draw current position of body
			BLIT(_surface, NULL, scr->surface(), &_bounds);
			break;
	}
	*/
	return geode;
}

void BodyViewOSG::positionChanged( Vect2f pos )
{
	//if (body()->objectName() == "MalletModel") {
	//	M_USER << "BodyPosChanged: " << pos << endl;
	//}

	if (body()->getShape() == BodyModel::BODY_SHAPE_CYLINDER) {
		_xform->setPosition( osg::Vec3(pos[0], pos[1], 0) );
		//double radius = body()->getRadius();
		//_xform->setScale( osg::Vec3(radius, radius, radius) );
		_xform->setAttitude( osg::Quat(body()->getAngle(), osg::Vec3f(0,0,1)) );
	}
}

