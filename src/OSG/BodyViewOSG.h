
#ifndef __METRIC_BODY_VIEW_OSG_H
#define __METRIC_BODY_VIEW_OSG_H 1

#include "Vect.h"

#include "BodyView.h"
#include "BodyModel.h"

#include "ScreenOSG.h"

namespace osg {
	class Group;
}

namespace metric {


class BodyViewOSG : public BodyView
{
public:
	BodyViewOSG( BodyModel *body,
			  Screen *screen );

	virtual ~BodyViewOSG();

	virtual BodyModel* body() {
		return dynamic_cast<BodyModel*>( View::model() );
	}
	virtual ScreenOSG* screen() {
		return dynamic_cast<ScreenOSG*>( View::screen() ); 
	}

	virtual void draw();
	virtual void setVisible(bool visible);

	// TODO: removed quickly to get compiled
	//void correct_pos(Vect2i &pos);
	//void get_screen_rect(Vect2f vPos, SDL_Rect &dstRect);

protected:
	virtual void positionChanged( Vect2f pos ); // slot

	osg::Group *_root;
	osg::Geode *_geode;
	osg::Geometry *_geometry;
	osg::PositionAttitudeTransform *_xform;

	Vect2f _maxVel; // for rendering velocity magnitudes

	osg::Geode* createAsCylinder();
	osg::Geode* createAsBox();
};

}

#endif

