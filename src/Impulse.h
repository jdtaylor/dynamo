
#ifndef __METRIC_IMPULSE_H
#define __METRIC_IMPULSE_H

#include "Object.h"
#include "BodyModel.h"

namespace metric {

class Engine;

class Impulse : public Object
{
public:
	Impulse( const Engine *engine,
			 BodyModel *body,
			 double duration,
			 Vect2f force,
			 Vect2f position=Vect2f() ) : // position of force relative to body's center of mass
		Object(),
		_engine( engine ),
		_body( body ),
		_position( position ),
		_force( force ),
   		_duration( duration )	{}
	virtual ~Impulse() {}

	void step( double dTime );
	double duration() const { return _duration; }
protected:

	const Engine *_engine;
	BodyModel *_body;
	Vect2f _position;
	Vect2f _force;
	double _duration;
};

}
#endif

