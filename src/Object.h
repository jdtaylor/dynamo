
#ifndef __METRIC_OBJECT_H
#define __METRIC_OBJECT_H 1

#include <string>

#include <sigc++/trackable.h>

namespace metric {

class Object : public sigc::trackable
{
public:
	Object(const std::string &name = "");
	virtual ~Object();

	virtual std::string objectName() const {
		return _objectName;
	}
	virtual void objectName(const std::string &name) {
		_objectName = name;
	}

private:
	std::string _objectName;
};

}

#endif

