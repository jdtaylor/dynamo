
#include "BodyModel.h"

namespace metric {

class SimulatedBodyModel : public BodyModel
{
public:
	SimulatedBodyModel();
	virtual ~SimulatedBodyModel();

	virtual Vect2f getPosition() const;
	virtual Vect2f getVelocity() const;
};

}

