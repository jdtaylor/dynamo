
#ifndef __SCREEN_FACTORY_H
#define __SCREEN_FACTORY_H

#include <algorithm>

#include <sigc++/signal.h>

#include "Object.h"
#include "Vect.h"
#include "Screen.h"
#include "BodyView.h"

namespace metric {

class ScreenDriver : public Object
{
public:
	ScreenDriver(int argc, char *argv[]);
	virtual ~ScreenDriver();

	virtual void exec() = 0;
	virtual bool loadResources() = 0;

	virtual Screen* screen() { return _screen; }

	virtual BodyView* makeBody(BodyModel *body, const std::string &) = 0;
	virtual void setControlledBodyView(BodyView *view) { _controlledBodyView = view; }

	void setDrawingMode(int mode)
	{
		for_each(_bodyViews.begin(), _bodyViews.end(), [=](BodyView *view) {
			view->setDrawingMode( mode );
		});
	}

	sigc::signal<void> preFrame() { return _sigPreFrame; }
	sigc::signal<void> postFrame() { return _sigPostFrame; }
	sigc::signal<void,const Vect2f&> mouseMoved() { return _sigMouseMove; }
	sigc::signal<void> restartSimulation() { return _sigRestartSimulation; }
	sigc::signal<void> shutdown() { return _sigShutdown; }
protected:
	Screen* _screen;
	BodyView *_controlledBodyView;

	typedef std::list<BodyView*> BodyViewList;
	BodyViewList _bodyViews;

	sigc::signal<void> _sigPreFrame;
	sigc::signal<void> _sigPostFrame;
	sigc::signal<void,const Vect2f&> _sigMouseMove;
	sigc::signal<void> _sigRestartSimulation;
	sigc::signal<void> _sigShutdown;
};

}


#endif
