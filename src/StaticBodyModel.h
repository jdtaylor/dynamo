
#ifndef __METRIC_STATIC_BODY_MODEL_H
#define __METRIC_STATIC_BODY_MODEL_H

#include "BodyModel.h"

namespace metric {

class StaticBodyModel : public BodyModel
{
public:
	StaticBodyModel();
	virtual ~StaticBodyModel();
};

}

#endif
