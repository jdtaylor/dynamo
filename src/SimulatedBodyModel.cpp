
#include "SimulatedBodyModel.h"
#include "Engine.h"

using namespace metric;

SimulatedBodyModel::SimulatedBodyModel() :
	BodyModel()
{
	BodyModel::setClass( BODY_CLASS_DYNAMIC ); // set simulated
}

SimulatedBodyModel::~SimulatedBodyModel()
{
}

Vect2f SimulatedBodyModel::getPosition() const
{
	return _pos;
	// 
	// This was a bad idea.  I'm now copying the positions 
	// from ODE to the bodies after every simulation frame.
	//
	//if (!engine())
	//	return Vect2f();
	//return engine()->getBodyPosition( this );
}

Vect2f SimulatedBodyModel::getVelocity() const
{
	return _vel;
	//if (!engine())
	//	return Vect2d();
	//return engine()->getBodyVelocity( this );
}

