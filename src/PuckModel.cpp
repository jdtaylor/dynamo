
#include "PuckModel.h"
#include "Sound.h"
#include "const.h"

using namespace metric;

PuckModel::PuckModel() :
	SimulatedBodyModel()
{
	setRadius( PUCK_RADIUS );
	setHeight( PUCK_HEIGHT );
	setMass( PUCK_MASS );
	setShape( BODY_SHAPE_CYLINDER );
	setExtents( Vect2f( PUCK_RADIUS, PUCK_RADIUS ) );
}

PuckModel::~PuckModel()
{
}

double PuckModel::getRotationalInertia() const
{
	// solid disk rotating like a wheel.
	// TODO: pucks have a thicker part around outer edge.
	double radius = getRadius();
	return 0.5 * getMass() * radius * radius;
}

void PuckModel::collisionOccured( const BodyModel *otherBody )
	//							  const Vect2f& pos, const Vect2f& norm )
{
	assert(otherBody);
	Vect2f otherPos = otherBody->getPosition();

	Sound::play("puck1");
}


