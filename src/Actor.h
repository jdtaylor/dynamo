
#ifndef __METRIC_ACTOR_H
#define __METRIC_ACTOR_H

#include "Object.h"

namespace metric {

class Engine;
class BodyModel;


class Actor : public Object
{
public:
	Actor( Engine *engine, BodyModel *body );
	virtual ~Actor();

	void step();

	Engine* engine() { return _engine; }
	BodyModel* body() { return _body; }

protected:
	Engine *_engine;
	BodyModel *_body;
};

}

#endif

