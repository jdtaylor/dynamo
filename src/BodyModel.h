
#ifndef __METRIC_BODY_MODEL_H
#define __METRIC_BODY_MODEL_H 1

#include <mutex>
#include <sigc++/sigc++.h>

#include "Model.h"
#include "Vect.h"
#include "Mat.h"
#include "Rect.h"

namespace metric {

class BodyModel : public Model
{
public:
	BodyModel( const std::string &name="" );
	virtual ~BodyModel();

	typedef enum {
		BODY_CLASS_DYNAMIC=0, // pure simulated
		BODY_CLASS_STATIC,    // infinite mass (static position)
		BODY_CLASS_CONTROLLED,// position controlled by user
		BODY_CLASS_TRIGGER,	  // reacts to collisions by emitting signals
		MAX_BODY_CLASS
	} BodyClass;

	typedef enum {
		BODY_SHAPE_CYLINDER=0,
		BODY_SHAPE_BOX,
		MAX_BODY_SHAPE
	} BodyShape;

	virtual bool simulated() { return _class == BODY_CLASS_DYNAMIC; }
	virtual bool controlled() { return _class == BODY_CLASS_CONTROLLED; }
	virtual BodyClass getClass() const { return _class; }
	virtual void setClass( BodyClass klass ) { _class = klass; }

	typedef unsigned long long collisionMask_t;
	virtual collisionMask_t collisionMask() const { return _collisionMask; }
	virtual void collisionMask( collisionMask_t c ) { _collisionMask = c; }
	virtual void collisionOccured( const BodyModel *otherBody );
	//virtual void collisionOccured( const BodyModel *otherBody,
	//							   const Vect2f& pos, const Vect2f& norm );

	virtual void setPosition(const Vect2f& newPos);
	virtual Vect2f getPosition() const;
	virtual Vect2f stepPosition( double dTime ) const;

	virtual Vect2f getVelocity() const;
	virtual void setVelocity( const Vect2f& vel );

	virtual double getAngle() const;
	virtual void setAngle(double angle);
	virtual double stepAngle( double dTime ) const;

	virtual double getAngularVelocity() const;
	virtual void setAngularVelocity(double angVel);

	virtual double distance(const Vect2f &pos) const;

	virtual Vect2f getLinearMomentum() const;
	virtual double getAngularMomentum() const;
	virtual void setAngularMomentum( double angMomentum );

	virtual double getKineticEnergy() const;
	virtual double getLinearKineticEnergy() const;
	virtual double getAngularKineticEnergy() const;

	virtual BodyShape getShape() const { return _shape; }
	virtual void setShape( BodyShape shape ) { _shape = shape; }

	virtual const Vect2f& getExtents() const { return _extents; }
	virtual void setExtents( const Vect2f &v ) { _extents = v; }

	virtual void setRadius(double radius);
	virtual double getRadius() const { return _radius; }
	virtual Vect2f getRadiusVec() const;

	virtual void setHeight(double height) { _height = height; }
	virtual double getHeight() const { return _height; }

	virtual void setMass(double mass);
	virtual double getMass() const { return _mass; }
	virtual double getRotationalInertia() const;

	virtual bool localToWorld( const Vect2f &local, Vect2f &world );
	virtual bool worldToLocal( const Vect2f &world, Vect2f &local );

	// signals
	sigc::signal<void,Vect2f>& positionChanged() { return _sigPositionChanged; }
	sigc::signal<void,Vect2f>& velocityChanged() { return _sigVelocityChanged; }

protected:
	BodyClass _class; //! whether body is simulated, static or controlled by user.
	BodyShape _shape; //! shape used by Engine for things like mass and inertia
	Vect2f _extents; //! half-extents (radius) of body in x and y world coord.
	double _radius; //! radius of body in meters
	double _height; //! height of body in meters
	double _mass; //! mass of body in kg
	Vect2f _pos; //! current position
	Vect2f _vel; //! current interval velocity
	double _angle; //! current angle orientation
	double _angVel; //! angular velocity
	collisionMask_t _collisionMask; //! bitmask for collidable bodies
	Mat2d _worldToLocal; // world -> local coord. transformation matrix

	// signals.  TODO: consider using 'const Vect2f &' here:
	sigc::signal<void,Vect2f> _sigPositionChanged;
	sigc::signal<void,Vect2f> _sigVelocityChanged;

	mutable std::mutex _mutex; //! locks position, velocity, and angular quantities
};

}

#endif

