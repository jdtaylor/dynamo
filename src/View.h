
#ifndef __METRIC_VIEW_H
#define __METRIC_VIEW_H 1

#include <vector>

#include "Object.h"
#include "Model.h"
#include "Screen.h"

namespace metric {

class View : public Object
{
public:
	View( Model* model, Screen* screen );
	virtual ~View();

	virtual Model* model() { return _model; }
	virtual Screen* screen() { return _screen; }

	bool invalid() const {return _model->invalid(); }
	void invalidate(bool b) { _model->invalidate( b ); }

protected:
	Model *_model;
	Screen *_screen;
};

}

#endif

