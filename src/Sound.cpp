
#include <algorithm>

#include <AL/alut.h>

#include "Sound.h"
#include "log.h"


using namespace std;
using namespace metric;

ALuint Sound::_buffers[MAX_BUFFERS];
std::vector<string> Sound::_bufferMap;
ALuint Sound::_sources[MAX_SOURCES];


bool Sound::init()
{
	alutInit(0, NULL);

	alGenBuffers(MAX_BUFFERS, _buffers);

	ALuint error;
	if((error = alGetError()) != AL_NO_ERROR) {
		M_ERROR << "alGenBuffers : " << error << endl;
		return false;
	}

	// Generate the sources
	alGenSources(MAX_SOURCES, _sources);
	if ((error = alGetError()) != AL_NO_ERROR) {
		M_ERROR << "alGenSources : " << error << endl;
		return false;
	}

	_bufferMap.clear();

	return true;
}

void Sound::destroy()
{
	alDeleteSources(MAX_SOURCES, _sources);
	alDeleteBuffers(MAX_BUFFERS, _buffers);
	_bufferMap.clear();

	alutExit();
}

bool Sound::load(string name, string path)
{
	ALenum     format;
	ALsizei    size;
	ALsizei    freq;
	ALboolean  loop;
	ALvoid*    data;
	ALuint error;

	if(find(_bufferMap.begin(), _bufferMap.end(), name) != _bufferMap.end()) {
		M_WARN << "Sound::load - resource already loaded: " << path << endl;
		return true; // already loaded
	}

	ALbyte alPath[1024];
	strncpy(reinterpret_cast<char*>(alPath), path.data(), sizeof(alPath));
	alutLoadWAVFile(alPath, &format, &data, &size, &freq, &loop);
	if((error = alGetError()) != AL_NO_ERROR) {
		M_ERROR << "alutLoadWAVFile " << path << ": " << error << endl;
		return false;
	}

	ALuint idx = _bufferMap.size();
	alBufferData(_buffers[idx], format, data, size, freq);
	if((error = alGetError()) != AL_NO_ERROR) {
		M_ERROR << "alBufferData buffer 0 : " << error << endl;
		return false;
	}


	alutUnloadWAV(format, data, size, freq);
	if ((error = alGetError()) != AL_NO_ERROR) {
		M_ERROR << "alutUnloadWAV : " << error << endl;
		return false;
	}

	alSourcei(_sources[idx], AL_BUFFER, _buffers[idx]);
	if ((error = alGetError()) != AL_NO_ERROR) {
		M_ERROR << "alSourcei : " << error << endl;
		return false;
	}

	//alSourcefv(_sources[idx], AL_POSITION, sourcePos);
	//alSourcefv(_sources[idx], AL_VELOCITY, sourceVel);
	//alSourcefv(_sources[idx], AL_DIRECTION, sourceOri);

	_bufferMap.push_back(name);

	return true;
}

ALuint Sound::getIdx(const string &name)
{
	std::vector<std::string>::iterator it;
	it = find(_bufferMap.begin(), _bufferMap.end(), name);
	if(it == _bufferMap.end()) {
		M_ERROR << "Unknown sound resource detected: " << name << endl;
		return 0;
	}

	ALuint idx = (ALuint)(it - _bufferMap.begin());
	if(idx >= _bufferMap.size() || idx >= MAX_BUFFERS) {
		M_ERROR << "Unknown sound resource index detected: " << idx << endl;
		return 0;
	}

	return static_cast<unsigned int>(idx);
}

bool Sound::play(string name)
{
	ALuint idx = getIdx(name);
	//alSourceRewind( _sources[idx] );
	alSourcePlay( _sources[idx] );
}

float Sound::duration(string name)
{
    ALint size, bits, channels, freq;
	ALuint buffer = getIdx(name);

    alGetBufferi(buffer, AL_SIZE, &size);
    alGetBufferi(buffer, AL_BITS, &bits);
    alGetBufferi(buffer, AL_CHANNELS, &channels);
    alGetBufferi(buffer, AL_FREQUENCY, &freq);
    if(alGetError() != AL_NO_ERROR) {
		M_ERROR << "Sound::duration: failed to get buffer info" << endl;
        return -1.0f;
	}

    return (float)((ALuint)size/channels/(bits/8)) / (float)freq;
}


