
#ifndef __METRIC_RECT_H
#define __METRIC_RECT_H

#include <iostream>

#include "Vect.h"

namespace metric {

template< class Type >
class Rect
{
public:
	typedef Type value_type;
	typedef Rect<Type> rect_type;
	typedef Vect<2,Type> vect_type;

	Rect() {}
	Rect( const vect_type &min, const vect_type &size ) {
		_min = min;
		_size = size;
	}
	Rect( const rect_type &rhs ) {
		_min = rhs._min;
		_size = rhs._size;
	}
	virtual ~Rect() {}

	rect_type& operator=( const rect_type &rhs ) {
		_min = rhs._min;
		_size = rhs._size;
		return *this;
	}

	bool operator==( const rect_type &rhs ) {
		return _min == rhs._min && _size == rhs._size;
	}

	std::ostream& operator>>(std::ostream& os) const
	{
		os << _min << "->" << _size;
		return os;
	}

	const vect_type& min() const { return _min; }
	const vect_type& size() const { return _size; }
	Type x() const { return _min[0]; }
	Type y() const { return _min[1]; }
	Type dx() const { return _size[0]; }
	Type dy() const { return _size[1]; }
	Type w() const { return _size[0]; } // aliases for dx(), dy()
	Type h() const { return _size[1]; }

protected:
	vect_type _min;
	vect_type _size;
};

template <class Type>
std::ostream& operator<<(std::ostream& os, const Rect<Type> &rect) { rect >> os; return os; }

typedef Rect<double> Rect2f;
typedef Rect<int> Rect2i;
typedef Rect<unsigned int> Rect2u;

}
#endif

