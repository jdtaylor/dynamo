
#ifndef __METRIC_STREAMABLE_H
#define __METRIC_STREAMABLE_H

#include <iostream>
#include <vector>

namespace metric {

// So I don't forget when I see this again:
//   This is to derive a model from to make it 
//   almost automatically streamable.  The idea 
//   is to declare all model member variables 
//   using the streamable template:
//      streamable<bool> _foo;
//   _foo will be recorded on construction and 
//   be available for serialization when requested.
//
//   I abandoned this because it will make 
//   debugging a nightmare!  Also, it isn't too 
//   difficult to create a stream( iostream& ) 
//   method for each model.
class Streamable
{
public:
	Streamable() {}
	virtual ~Streamable() {}

	/* Disabling for compilation problems on Ubuntu
	 *
	template <class T>
	class streamable
	{
	public:
		streamable() {
			idx = addStreamable( new T() );
		}

		T& operator()() {
			return *dynamic_cast<T*>( getStreamable(idx) );
		}
	protected:
		unsigned int idx;
	};

	unsigned int addStreamable( void *s )
	{
		_streamList.push_back( s );
		return _streamList.size() - 1;
	}

	void* getStreamable( unsigned int idx ) {
		return _streamList[idx];
	}

	std::ostream& operator<<(std::ostream& ost);

protected:

	std::vector<void*> _streamList;
	*/
};

}

#endif

