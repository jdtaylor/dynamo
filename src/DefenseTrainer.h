
#ifndef __METRIC_DEFENSE_TRAINER_H
#define __METRIC_DEFENSE_TRAINER_H

#include "Object.h"

namespace metric {

class Engine;
class BodyModel;
class ScreenDriver;

class DefenseTrainer : public Object
{
public:
	DefenseTrainer( Engine *engine,
					ScreenDriver *screenDriver );
	virtual ~DefenseTrainer();

	void step();

	Engine* engine() { return _engine; }
	BodyModel* body() { return _body; }

	typedef enum {
		SHOT_TYPE_STRAIGHT,
		SHOT_TYPE_CROSS,
		SHOT_TYPE_RWO,
		SHOT_TYPE_RWU,
		NUM_SHOT_TYPE
	} shotType_t;

	void launchPuck( shotType_t shotType,
					 double velocity );

protected:
	Engine *_engine;
	ScreenDriver *_screenDriver;
	BodyModel *_body;
};

}

#endif

