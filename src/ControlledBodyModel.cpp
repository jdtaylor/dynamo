
#include "ControlledBodyModel.h"
#include "const.h"

using namespace metric;

//
// This controls the window size, in time, that input 
// mallet velocities are averaged with.  The default of 
// 90% means that incoming interval velocities contribute 
// 10% to the current mallet velocity.  eg. It will take 
// 10 engine frames to keep the mallet perfectly still
// after moving around.
//
#define MALLET_RESPONSE_PERCENT 0.9


ControlledBodyModel::ControlledBodyModel(const std::string &name) :
	BodyModel(name)
{
	setClass( BODY_CLASS_CONTROLLED );
}

ControlledBodyModel::~ControlledBodyModel()
{
}

void ControlledBodyModel::setControlPosition( const Vect2f& newPos )
{
	// Save new position so the Engine can perform velocity 
	// derivation and collision detection on controlled bodies.
	_mutex.lock();
	_controlPos = newPos;
	_mutex.unlock();

	/* NOTE: This is now done in the engine 
	Vect2f dPos = newPos - getPosition();
	_controlVel = 
			getVelocity() * MALLET_RESPONSE_PERCENT +
			dPos * ((1.0 - MALLET_RESPONSE_PERCENT) / engine()->getFrameDuration());

	BodyModel::setVelocity( _controlVel );
	*/

	//BodyModel::setPosition( _controlPos );
	//BodyModel::setAngle( atan2( _controlVel[1], _controlVel[0] ) );
//	engine()->setBodyPosition( this, _controlPos, _controlVel );
}

Vect2f ControlledBodyModel::getControlPosition() const
{
	Vect2f curControlPos;

	_mutex.lock();
	curControlPos = _controlPos;
	_mutex.unlock();

	return curControlPos;
}

