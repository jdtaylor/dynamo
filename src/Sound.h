
#ifndef __METRIC_SOUND_H
#define __METRIC_SOUND_H 1

#include <string>
#include <vector>

#include <AL/al.h>

#include "Object.h"

namespace metric {

#define MAX_BUFFERS 32
#define MAX_SOURCES 32

class Sound : public Object
{
public:
	virtual ~Sound();
	static bool init();
	static void destroy();
	static bool load(std::string name, std::string path);
	static bool play(std::string name);
	static float duration(std::string name);

protected:
	static ALuint _buffers[MAX_BUFFERS];
	static std::vector<std::string> _bufferMap;
	static ALuint _sources[MAX_SOURCES];

	static ALuint getIdx(const std::string &name);

private:
	Sound();
	Sound(const Sound&);
	Sound& operator =(const Sound&);
};

}

#endif

