
#ifndef __METRIC_EXCEPTION_H
#define __METRIC_EXCEPTION_H

#include <stdexcept>

//!
// Out Of Bounds exception
//
class OobException : public std::out_of_range {
public:
	OobException() throw() :
		std::out_of_range("Out of bounds exception") {}
	OobException(std::string msg) throw() :
		std::out_of_range(msg) {}
	virtual ~OobException() throw() {}
};

class DivZeroException : public std::runtime_error {
public:
	DivZeroException() throw() :
		std::runtime_error("Divide by zero exception") {}
	DivZeroException(std::string msg) throw() : 
		std::runtime_error(msg) {}
	virtual ~DivZeroException() throw() {}
};


#endif

