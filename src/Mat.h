
#ifndef __METRIC_MAT_H
#define __METRIC_MAT_H

#include "Object.h"
#include "Vect.h"
#include "Exception.h"
#include "const.h"

namespace metric {

//
// Mat[row_selection][column_selection]
//
template< unsigned int Degree, class Type >
class Mat : public Object
{
public:
	typedef Type value_type;
	typedef Mat<Degree,Type> mat_type;

	Mat() {
		// default to identity matrix
		for(unsigned int i = 0; i < Degree; ++i) {
			m[i][i] = 1.0;
		}
	}

	virtual ~Mat() {}

	Type operator[](unsigned int i) const {
		if(i >= Degree)
			throw OobException();
		return m[i];
	}

	Type& operator[](unsigned int i) {
		if(i >= Degree)
			throw OobException();
		return m[i];
	}

	mat_type& operator=(const mat_type &rhs) {
		for(unsigned int i=0; i < Degree; ++i)
			m[i] = rhs[i];
		return *this;
	}

	mat_type operator*(const mat_type& rhs) const {
		mat_type u;
		for(unsigned int i = 0; i < Degree; i++)
			u.m[i] = m[i] * rhs[i];
		return u;
	}

	mat_type operator*(const Type &val) const {
		mat_type u;
		for(unsigned int i = 0; i < Degree; i++)
			u.m[i] = m[i] * val;
		return u;
	}

	mat_type& operator*=(const mat_type& rhs) {
		for(unsigned int i = 0; i < Degree; ++i)
			m[i] *= rhs.m[i];
		return *this;
	}

	mat_type& operator*=(const Type &val) {
		for(unsigned int i = 0; i < Degree; ++i)
			m[i] *= val;
		return *this;
	}

	mat_type operator*(const Vect<Degree,Type>& v) const {
		mat_type u;
		// [row][column]
		for(unsigned int i = 0; i < Degree; i++)
			u.m[i] = m[i] * v;
		return u;
	}

	Vect<Degree,Type> transform(const Vect<Degree,Type>& v) const {
		Vect<Degree,Type> u;
		for(unsigned int i = 0; i < Degree; i++) {
			Vect<Degree,Type> vi = m[i] * v;
			u[i] = vi.sum(); // sum all components
		}
		return u;
	}

	std::ostream& operator>>(std::ostream& os) const
	{
		os << "[";
		for(unsigned int i = 0; i < Degree-1; ++i)
			os << m[i] << ",";
		os << m[Degree-1] << "]";
		return os;
	}

	mat_type transpose() const {
		mat_type u;
		for(unsigned int i = 0; i < Degree; i++)
			for(unsigned int j = 0; j < Degree; j++)
				u.m[i][j] = m[j][i];
		return u;
	}

	mat_type inverse() const {
		assert(Degree == 2);
		mat_type u;
		double det = m[0][0]*m[1][1] - m[0][1]*m[1][0];
		if (std::abs(det) < EPSILON) {
			throw DivZeroException("Mat::transpose Division by zero");
			return u;
		}
		u.m[0][0] = m[1][1] / det;
		u.m[0][1] = - m[0][1] / det;
		u.m[1][0] = - m[1][0] / det;
		u.m[1][1] = m[0][0] / det;
		return u;
	}

protected:
	Vect< Degree, Vect<Degree,Type> > m;
};

template <unsigned int Degree, class Type>
std::ostream& operator<<(std::ostream& os, const Mat<Degree,Type> &vec) { vec >> os; return os; }

typedef Mat<2,float> Mat2f;
typedef Mat<2,double> Mat2d;
typedef Mat<2,int> Mat2i;
typedef Mat<2,unsigned int> Mat2u;

typedef Mat<3,float> Mat3f;
typedef Mat<3,double> Mat3d;
typedef Mat<3,int> Mat3i;
typedef Mat<3,unsigned int> Mat3u;

}
#endif

