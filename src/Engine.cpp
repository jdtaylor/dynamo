
#include <algorithm>
#include <sys/time.h>
#include <unistd.h>

#include "ode/ode.h"

#include "const.h"
#include "Rect.h"
#include "SimulatedBodyModel.h"
#include "ControlledBodyModel.h"
#include "Engine.h"
#include "Impulse.h"

using namespace std;
using namespace metric;

Engine::Engine( Rect2f world ) :
	Object(),
	_world( world ),
	_running(true),
	_desiredFramerate( 30.0 ),
	_actualFramerate( 0.0 ),
	_desiredFrameDuration( 0 ),
	_actualFrameDuration( 0 )
{
	_startTime = calcCurrentTime();
	_finishTime = _startTime;

	dInitODE(); // TODO: dSetErrorHandler is called before this
	_worldId = dWorldCreate();

	dWorldSetGravity( _worldId, 0.0, 0.0, 0.0 );

	_spaceId = dSimpleSpaceCreate( 0 );

	_contactJointGroup = dJointGroupCreate(0);

	// TODO: consider doing this on a per-body basis
	dWorldSetAutoDisableFlag( _worldId, 0 );

	dWorldSetERP(_worldId, 0.8); //0.2);
//    dWorldSetCFM(_worldId, 0.0); //1e-5);
	createTable();
}

Engine::~Engine()
{
	dJointGroupDestroy( _contactJointGroup );
	dSpaceDestroy(_spaceId);
	dWorldDestroy(_worldId);
	dCloseODE();
}

microsec_t Engine::calcCurrentTime() const
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return static_cast<microsec_t>( tv.tv_sec * 1000000 + tv.tv_usec );
}

void Engine::setFramerate( double fps )
{
	if (fabsf( fps ) < 1.0) { 
		M_ERROR << "Frames Per Second too low: " << fps << endl;
		return;
	}
	_desiredFramerate = fps;
	_desiredFrameDuration = static_cast<microsec_t>( 1000000.0 / _desiredFramerate );
}

void Engine::startFrame()
{
	_startTime = calcCurrentTime();
}

void Engine::finishFrame()
{
	_finishTime = calcCurrentTime();
	if (_finishTime < _startTime) {
		M_ERROR << "Finish Time occured before Start Time." << endl;
		_startTime = _finishTime;
		return;
	}

	microsec_t actualFrameDuration = _finishTime - _startTime;
	if (_actualFrameDuration > _desiredFrameDuration) {
		M_ERROR << "This computer is too slow for this framerate. Aborting" << endl;
		exit(0);
	}
	microsec_t timeLeft = _desiredFrameDuration - _actualFrameDuration;

	usleep( static_cast<useconds_t>(timeLeft) );

	_actualFramerate = 1000000.0 / double(actualFrameDuration);
}

void Engine::exec()
{
	while(_running)
	{
		startFrame();

		preStep().emit();

		syncControlledBodies();

		step();

		syncSimulatedBodies();

		postStep().emit();

		finishFrame();
	}
}

bool Engine::registerBody( BodyModel *bodyModel )
{
	if ( std::find( _bodies.begin(), _bodies.end(), bodyModel ) != _bodies.end() ) {
		M_ERROR << "Tried to register body twice" << endl;
		return false;
	}
	// TODO: if body is deleted.. might want a signal in its destructor
	_bodies.push_back( bodyModel );

	// Create analog of body in ODE:
	odeBody &body = createBody();

	body.model = bodyModel;
	_bodyModelMap.insert( BodyModelMap::value_type(bodyModel, &body) );

	static int bodyCount = 0;
	dBodySetData( body.id, reinterpret_cast<void*>(bodyCount++) );

	// GUI measures extents in half-dimensions, so get whole extents:
	Vect2f vDim = bodyModel->getExtents() * 2.0;

	switch (bodyModel->getShape())
	{
	default:
	case BodyModel::BODY_SHAPE_CYLINDER:
		// create the dMass object to a cylindrical distribution:
		dMassSetCylinderTotal( &body.mass,
				bodyModel->getMass(), // total mass
				3, // direction is along z-axis (1=x,2=y,3=z)
				bodyModel->getRadius(),
				bodyModel->getHeight() ); // real height is important for determining inertia.
		break;
	case BodyModel::BODY_SHAPE_BOX:
		// create the dMass object to a cylindrical distribution:
		dMassSetBoxTotal( &body.mass,
				//1000.0, // TODO: total mass matters if kinematic?
				1.0,
				vDim[0], vDim[1], 1.0 );
		break;
	}

	// set dMass object to the body:
	dBodySetMass( body.id, &body.mass );

	Vect2f pos = bodyModel->getPosition();
	dBodySetPosition( body.id, pos[0], pos[1], 0.0 );

	Vect2f vel = bodyModel->getVelocity();
	dBodySetLinearVel( body.id, vel[0], vel[1], 0.0 );

	switch (bodyModel->getClass()) {
	default:
	case BodyModel::BODY_CLASS_DYNAMIC:
	case BodyModel::BODY_CLASS_CONTROLLED:
		dBodySetDynamic( body.id );
		// 1: A ``finite orientation update is used. This is more costly to compute, but will be
		// more accurate for high speed rotations. Note however that high speed rotations can result
		// in many types of error in a simulation, and this mode will only fix one of those sources
		// of error:
		dBodySetFiniteRotationMode( body.id, 1 );
		dBodySetFiniteRotationAxis( body.id, 0, 0, 1.0 );
		dBodySetLinearDamping( body.id, 0.0 );
		dBodySetAngularDamping( body.id, 0.0 );
		break;
	case BodyModel::BODY_CLASS_STATIC:
		// Kinematic objects fail to be controlled when operated via motors.
		dBodySetKinematic( body.id ); // don't react to simulation (infinite mass)
		break;
	}

	switch (bodyModel->getShape())
	{
	default:
	case BodyModel::BODY_SHAPE_CYLINDER:
		// ODE doesn't support cylinder-cylinder collisions, so use sphere:
		//body.geom = dCreateCylinder( _spaceId, body->getRadius(), 1.0 );
		body.geom = dCreateSphere( _spaceId, bodyModel->getRadius() );
		break;
	case BodyModel::BODY_SHAPE_BOX:
		body.geom = dCreateBox( _spaceId, vDim[0], vDim[1], 1.0 );
		break;
	}

	dGeomSetBody( body.geom, body.id ); // link body/geom position and orientation 

	body.planeJoint = dJointCreatePlane2D( _worldId, 0 );
	if (bodyModel->getClass() == BodyModel::BODY_CLASS_CONTROLLED) {
		// Use 2D motors for the controlled bodies (mallet).  Setting the 
		// velocity and position before each sim step doesn't work, so we 
		// use an effective impulse via the motors before each step.
		// Setting fmax to zero turns off the motor.  We don't want to turn 
		// the motor on for simulated bodies as they need to move around freely.
		dJointSetPlane2DXParam(body.planeJoint, dParamFMax, 10000.0);
		dJointSetPlane2DYParam(body.planeJoint, dParamFMax, 10000.0);
		dJointSetPlane2DAngleParam(body.planeJoint, dParamLoStop, 0.0 );
		dJointSetPlane2DAngleParam(body.planeJoint, dParamHiStop, 0.0 );
	}
	dJointAttach( body.planeJoint, body.id, 0 );

	return true;
}

void Engine::nearCollisionCallback( void *data, dGeomID o1, dGeomID o2 )
{
	if (dGeomIsSpace (o1) || dGeomIsSpace (o2)) { 

		// colliding a space with something :
		dSpaceCollide2 (o1, o2, data, &nearCollisionCallback); 

		// collide all geoms internal to the space(s)
		if (dGeomIsSpace (o1))
			dSpaceCollide ((dSpaceID)o1, data, &nearCollisionCallback);
		if (dGeomIsSpace (o2))
			dSpaceCollide ((dSpaceID)o2, data, &nearCollisionCallback);
	} else {
		Engine *self = static_cast<Engine*>(data);
		if (self) {
			self->nearCollisionCallback( o1, o2 );
		}
	}
}

void Engine::nearCollisionCallback( dGeomID o1, dGeomID o2 )
{
	dBodyID bodyId1 = dGeomGetBody(o1);
	dBodyID bodyId2 = dGeomGetBody(o2);
	if (_bodyIdMap.find( bodyId1 ) == _bodyIdMap.end() ||
		_bodyIdMap.find( bodyId2 ) == _bodyIdMap.end())
	{
		// All ODE bodies should be mapped to a odeBody
		return;
	}
	const odeBody *body1 = _bodyIdMap[bodyId1];
	const odeBody *body2 = _bodyIdMap[bodyId2];

	if (!body1 || !body2) {
		// All ODE bodies should be mapped to a odeBody
		return;
	}

	dContact contacts[1000];
	int skip = sizeof(dContact);
	int maxContacts = sizeof(contacts) / skip;

	//const dReal *vPos1 = dBodyGetPosition( bodyId1 );
	//const dReal *vPos2 = dBodyGetPosition( bodyId2 );
	//if (vPos1 && vPos2) {
	//	M_DUMP << "1: [" << vPos1[0] << ", " << vPos1[1] << ", " << vPos1[2] << "]" << endl;
	//	M_DUMP << "2: [" << vPos2[0] << ", " << vPos2[1] << ", " << vPos2[2] << "]" << endl;
	//}

	// colliding two non-space geoms, so generate contact
	// points between o1 and o2
	int nContact = dCollide (o1, o2, maxContacts, &contacts[0].geom, skip);

	if (nContact > 0)
	{
		if (body1->model && body2->model) {
			BodyModel *model1 = body1->model;
			BodyModel *model2 = body2->model;
			/*
			// Still emit collisions if not collidable for creating 
			// trigger pads that don't react physically to collisions.
			// NOTE: I removed trigger pad functionality for a couple reasons:
			// 	1. Too many collisions 
			// 	2. Ambiguous collisions.  
			collision().emit(model1, model2);
			*/
			body1->model->collisionOccured( model2 );
			body2->model->collisionOccured( model1 );
		}
		//M_DUMP << "Collisions: " << nContact << endl;

		for (int i = 0; i < nContact; ++i)
		{
			dContact *c = &contacts[i];
			dContactGeom *cgeom = &c->geom;

			/* TODO: This sorta worked for emitting collisions: 
			if (body1->model && body2->model) {

				M_DUMP << "got collision" << endl;
				body1->model->collisionOccured( body2->model,
						Vect2f(cgeom->pos[0], cgeom->pos[1]),
						Vect2f(cgeom->normal[0], cgeom->normal[1]) );
				body2->model->collisionOccured( body1->model,
						Vect2f(cgeom->pos[0], cgeom->pos[1]),
						Vect2f(cgeom->normal[0], cgeom->normal[1]) );
			}
			*/
			/*
			{
				dContactGeom *g = &c->geom;
				M_DUMP << " pos:  [" << g->pos[0] << ", " << g->pos[1] << ", " << g->pos[2] << "]" << endl;
				M_DUMP << " norm: [" << g->normal[0] << ", " << g->normal[1] << ", " << g->normal[2] << "]" << endl;
				M_DUMP << " depth: " << g->depth << endl;
			}
			*/

			c->surface.mode = dContactBounce; // | dContactSoftCFM;
			c->surface.mu = dInfinity;
			c->surface.mu2 = 0.0;
			c->surface.bounce = 0.8;
			c->surface.bounce_vel = 0.0;

			dJointID j = dJointCreateContact(_worldId, _contactJointGroup, c);
			dJointAttach(j, bodyId1, bodyId2);
		}
	}
}

void Engine::syncControlledBodies()
{
	//
	// Copy new position and velocities from ODE to simulated bodies
	//
	auto it = _bodyModelMap.begin();
	for( ; it != _bodyModelMap.end(); ++it ) {
		BodyModel *bodyModel = it->first;
		odeBody *body = it->second;

		ControlledBodyModel *cb = dynamic_cast<ControlledBodyModel*>( bodyModel );

		if (!bodyModel->simulated() && cb)
		{
			//const dReal *vRot = dBodyGetRotation( body->id );
			Vect2f oldPos( dBodyGetPosition(body->id) );
			Vect2f newPos = cb->getControlPosition();
			Vect2f vel = (newPos - oldPos) / getFrameDuration();

			dJointSetPlane2DXParam(body->planeJoint, dParamVel, vel[0]);
			dJointSetPlane2DYParam(body->planeJoint, dParamVel, vel[1]);
			//dJointSetPlane2DAngleParam(body->planeJoint, dParamVel, atan2(vRot[4], vRot[0]) );

			/*
			   Vect2f pos = bodyModel->getPosition();
			   dBodySetPosition( body->id, pos[0], pos[1], 0.0 );

			   Vect2f vel = bodyModel->getVelocity();
			   dBodySetLinearVel( body->id, vel[0], -vel[1], 0.0 );

			   dQuaternion identity;
			   dQSetIdentity( identity );
			   dBodySetQuaternion( body->id, identity );

			   dBodySetAngularVel( body->id, 0.0, 0.0, 0.0 );
			 */
		}
	}
}

void Engine::syncSimulatedBodies()
{
	//
	// Copy new position and velocities from ODE to simulated bodies
	//
	auto it = _bodyModelMap.begin();
	for( ; it != _bodyModelMap.end(); ++it ) {
		BodyModel *bodyModel = it->first;
		odeBody *body = it->second;

		// correct any drift off of table top:
		alignBodyToZAxis( body->id );

		// Always use controlled-body positions from user.  They 
		// have an ODE position, but I don't want this affecting 
		// the rendering of where the user places the body.
//		if (bodyModel->simulated())
		{
			const dReal *vPos = dBodyGetPosition( body->id );
			const dReal *vVel = dBodyGetLinearVel( body->id );
			const dReal *vRot = dBodyGetRotation( body->id );
			const dReal *vAng = dBodyGetAngularVel( body->id );
			if (!vPos || !vVel || !vRot || !vAng)
				continue;
			//M_DUMP << "set pos: [" << vPos[0] << ", " << vPos[1] << ", " << vPos[2] << "]" << endl;
			//M_DUMP << "ang vel: " << vAng[2] * 180.0 / M_PI << std::endl;
			//M_DUMP << "euler: " << atan2(vRot[9],vRot[10]) << ", " <<
			//					 asin( -vRot[8] ) << ", " <<
			//					 atan2(vRot[4], vRot[0]) << endl;
			
			//if (!bodyModel->simulated()) {
			//	M_DUMP << "pso: " << vPos[0] << ", " << vPos[1] << " vel: [" << vVel[0] << ", " << vVel[1] << "]" << endl;
			//}

			// dump mallet position:
			//if (!bodyModel->simulated() && dynamic_cast<ControlledBodyModel*>( bodyModel )) {
			//	M_USER << "Simulated to: " << Vect2f(vPos[0],vPos[1]) << endl;
			//}
			bodyModel->setPosition( Vect2f(vPos) );
			bodyModel->setVelocity( Vect2f(vVel) );
			bodyModel->setAngle( atan2(vRot[4], vRot[0]) );
			bodyModel->setAngularVelocity( vAng[2] );

			// Clockwise == negative, Counter-clockwise == positive
			// (right-handed to positive-Z axis)
			//double angle = atan2(vRot[4], vRot[0]);
			//M_DUMP << "angle: " << angle << " angVel: " << vAng[2] << endl;
		}
	}
}

void Engine::step()
{
	// Step ODE:
	dSpaceCollide( _spaceId, this, &nearCollisionCallback );
	dWorldStep( _worldId, getFrameDuration() );
	dJointGroupEmpty( _contactJointGroup );

	/*
	for_each(_impulses.begin(), _impulses.end(), [=](Impulse &impulse) {
		impulse.step( _desiredFrameDuration );
	});
	remove_if(_impulses.begin(), _impulses.end(), [](Impulse &impulse) {
		return impulse.duration() <= 0.0;
	});
	*/
}

Engine::odeBody& Engine::createPlane(double a, double b, double c, double d)
{
	odeBody *body = new odeBody();
	body->geom = dCreatePlane( _spaceId, a, b, c, d ); // bot
	body->id = dGeomGetBody(body->geom);
	_bodyIdMap.insert( BodyIdMap::value_type(body->id, body) );
	return *body;
}

Engine::odeBody& Engine::createBody()
{
	odeBody *body = new odeBody();
	body->id = dBodyCreate( _worldId );
	_bodyIdMap.insert( BodyIdMap::value_type(body->id, body) );
	return *body;
}

void Engine::createTable()
{
	createPlane(  0,  1,  0,	0 ); // bot
	createPlane(  0, -1,  0,	-world().dy() ); // top
	createPlane(  1,  0,  0,	0 ); // left
	createPlane( -1,  0,  0,	-world().dx() ); // right

	/*
	double goalLength = 0.1;
	double goalWidth = 0.4;
	double wallWidth = (world().dx() - goalWidth) / 2.0;

	double tableWidth = world().dx();
	double tableLength = world().dy();

	Vect2f vDim( wallWidth, goalLength );

	createBox( Vect2f( 0.0, tableLength - goalLength ), vDim );
	createBox( Vect2f( tableWidth - wallWidth, tableLength - goalLength ), vDim );
	createBox( Vect2f( 0.0, 0.0 ), vDim );
	createBox( Vect2f( tableWidth - wallWidth, 0.0 ), vDim );
	*/
}

Engine::odeBody& Engine::createBox( Vect2f _vMin, Vect2f _vDim )
{
	// convert from 2D to 3D by extruding Z-Axis from -0.5 to 0.5
	Vect3f vMin( _vMin[0], _vMin[1], -0.5 );
	Vect3f vDim( _vDim[0], _vDim[1], 1.0 );
	Vect3f vMax = vMin + vDim;
	Vect3f vCenter = (vMax + vMin) / 2.0;

	// Create analog of body in ODE:
	odeBody &body = createBody();

	// create the dMass object to a cylindrical distribution:
	dMassSetBoxTotal( &body.mass,
			1000.0, // TODO: total mass matters if kinematic?
			vDim[0], vDim[1], vDim[2] );

	// set dMass object to the body:
	dBodySetMass( body.id, &body.mass );

	dBodySetPosition( body.id, vCenter[0], vCenter[1], vCenter[2] );
	dBodySetLinearVel( body.id, 0.0, 0.0, 0.0 );
	dBodySetAutoDisableFlag( body.id, 1 );
	dBodySetKinematic( body.id ); // don't react to simulation (infinite mass)

	body.geom = dCreateBox( _spaceId, vDim[0], vDim[1], vDim[2] );

	dGeomSetBody( body.geom, body.id ); // link body/geom position and orientation 
	return body;
}

Vect2f Engine::getBodyPosition( BodyModel *body ) const
{
	Vect2f pos;

	auto iter = _bodyModelMap.find( body );
	if ( iter == _bodyModelMap.end() )
		return pos;

	const dReal *odePos = dBodyGetPosition( iter->second->id );
	if (!odePos)
		return pos;

	pos[0] = odePos[0];
	pos[1] = odePos[1];

	return pos;
}

Vect2f Engine::getBodyVelocity( BodyModel *body ) const
{
	Vect2f vel;

	auto iter = _bodyModelMap.find( body );
	if ( iter == _bodyModelMap.end() )
		return vel;

	const dReal *odeVel = dBodyGetLinearVel( iter->second->id );
	if (!odeVel)
		return vel;

	vel[0] = static_cast<double>( odeVel[0] );
	vel[1] = static_cast<double>( odeVel[1] );

	return vel;
}

//
// Copied from the most-excellent wiki page on restricting objects to 2D:
// http://ode-wiki.org/wiki/index.php?title=HOWTO_constrain_objects_to_2d
//
void Engine::alignBodyToZAxis( dBodyID bodyId )
{
    const dReal *rot = dBodyGetAngularVel( bodyId );
    const dReal *quat_ptr;
    dReal quat[4], quat_len;
    quat_ptr = dBodyGetQuaternion( bodyId );
    quat[0] = quat_ptr[0];
    quat[1] = 0;
    quat[2] = 0; 
    quat[3] = quat_ptr[3]; 
    quat_len = sqrt( quat[0] * quat[0] + quat[3] * quat[3] );
    quat[0] /= quat_len;
    quat[3] /= quat_len;
    dBodySetQuaternion( bodyId, quat );
    dBodySetAngularVel( bodyId, 0, 0, rot[2] );
}


