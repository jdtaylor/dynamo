
#include "BodyModel.h"

namespace metric {

class ControlledBodyModel : public BodyModel
{
public:
	ControlledBodyModel(const std::string &name="");
	virtual ~ControlledBodyModel();

	virtual void setControlPosition(const Vect2f& newPos);
	virtual Vect2f getControlPosition() const;

protected:
	Vect2f _controlPos; // latest mouse-controlled position
	Vect2f _controlVel; // latest mouse-controlled velocity
};

}

