
#ifndef __METRIC_SCREEN_H
#define __METRIC_SCREEN_H 1

#include "Object.h"
#include "Rect.h"
#include "Color.h" // TODO: These really need to be nested in 'metric' subdir

namespace metric {

class Screen : public Object
{
public:
	Screen( int argc, char *argv[] ) : _bgColor(0,0,0) {}
	virtual ~Screen() {}

	virtual bool open( const std::string &windowTitle ) = 0;
	virtual bool close() = 0;

	virtual unsigned int width() const = 0;
	virtual unsigned int height() const = 0;
	virtual Rect2f world() const = 0;

	virtual void setBgColor(const Color &c) { _bgColor = c; }
	virtual void clear() {}

protected:
	Color _bgColor; // background color if not bitmap
};

}

#endif

