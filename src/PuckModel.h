
#include "SimulatedBodyModel.h"

namespace metric {

class PuckModel : public SimulatedBodyModel
{
public:
	PuckModel();
	virtual ~PuckModel();

	virtual double getRotationalInertia() const;

	virtual void collisionOccured( const BodyModel *otherBody );
								   //const Vect2f& pos, const Vect2f& norm );
};

}

