
#ifndef __METRIC_CONST_H
#define __METRIC_CONST_H

#define FEET_PER_METER 3.281
#define EPSILON 0.00001

// TODO: I don't know where these go:
//#define SCREEN_WIDTH 800
//#define SCREEN_HEIGHT 600
//#define SCREEN_WIDTH 1680 // luna
//#define SCREEN_WIDTH 800 // pluto
//#define SCREEN_WIDTH 525
//#define SCREEN_HEIGHT 1050 // luna
//#define SCREEN_HEIGHT 500 // pluto
//#define SCREEN_WIDTH 312
//#define SCREEN_HEIGHT 600

#define SCREEN_HEIGHT 1024 // luna
#define SCREEN_WIDTH (SCREEN_HEIGHT) // / 2)

#define SCREEN_RATIO ((double)SCREEN_WIDTH/SCREEN_HEIGHT)


// TODO: put these in their place:
//
// table width:  1.349 meters
// table length: 2.59 meters
//
#define TABLE_WIDTH (4.0 / FEET_PER_METER) // aprox size of table in meters
//#define TABLE_LENGTH (8.0 / FEET_PER_METER)  // full 8 foot table
//double g_tableLength = 4.0 / FEET_PER_METER;  // half court
// same aspect ratio as screen:
#define TABLE_LENGTH (TABLE_WIDTH / SCREEN_RATIO)
//

//#define MALLET_RADIUS ((4.00/12.0/2.0) / FEET_PER_METER) // 4 inches
#define MALLET_RADIUS ((1.0/12.0/2.0) / FEET_PER_METER) // Making mallet size of cursor
#define MALLET_HEIGHT ((2.0/12.0/2.0) / FEET_PER_METER) // 2 inches
#define MALLET_MASS   (170.0 / 1000.0) // 170g is a bit less than 6oz
#define PUCK_RADIUS   ((3.25/12.0/2.0) / FEET_PER_METER) // 3.25 inches
#define PUCK_HEIGHT   ((0.25/12.0/2.0) / FEET_PER_METER) // 0.25 inch
#define PUCK_MASS	  (MALLET_MASS / 2.0) // TODO: I'm guessing here

#define PUCK_LINEAR_DAMPING 1.0
#define PUCK_ANGULAR_DAMPING 0.0
//#define PUCK_LINEAR_DAMPING 6.0
//#define PUCK_ANGULAR_DAMPING 6.0

//#define MALLET_FRICTION 0.1
#define MALLET_FRICTION 0.0
//#define MALLET_RESTITUTION 2.1
#define MALLET_RESTITUTION 1.0

#define PUCK_FRICTION 0.1
//#define PUCK_RESTITUTION 2.1
#define PUCK_RESTITUTION 1.0  // should be between 0 and 1


#define APP_TITLE "Air Hockey"

#endif

