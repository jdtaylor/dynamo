
#ifndef __OI_TABLE_CAMERA_H
#define __OI_TABLE_CAMERA_H

#include "Inventor/nodes/SoOrthographicCamera.h"

namespace metric {

class TableCamera : public SoOrthographicCamera
{
public:
	TableCamera();

protected:
	virtual ~TableCamera();
};

}

#endif
