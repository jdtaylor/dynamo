
#include <sigc++/sigc++.h>

#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/nodes/SoTransform.h>
#include <Inventor/nodes/SoCylinder.h>
#include <Inventor/events/SoKeyboardEvent.h>
#include <Inventor/events/SoLocation2Event.h>

#include "const.h"
#include "ScreenDriverOI.h"
#include "BodyViewOI.h"

using namespace metric;


ScreenDriverOI::ScreenDriverOI(int argc, char *argv[]) :
	ScreenDriver(argc, argv)
{
	_screen = new ScreenOI( argc, argv );

	_keyboardEventCallback = new SoEventCallback();
	_keyboardEventCallback->addEventCallback( SoKeyboardEvent::getClassTypeId(),
									 	 	  fnKeyboardEventCB, this );
	screen()->addChild( _keyboardEventCallback );

	_mouseMotionEventCallback = new SoEventCallback();
	_mouseMotionEventCallback->addEventCallback( SoLocation2Event::getClassTypeId(),
									 	 	  fnMouseMotionEventCB, this );
	screen()->addChild( _mouseMotionEventCallback );
}

ScreenDriverOI::~ScreenDriverOI()
{
	delete _screen;
}

void ScreenDriverOI::exec()
{
	SoQt::mainLoop();
}

bool ScreenDriverOI::loadResources()
{
	return true;
}

BodyView* ScreenDriverOI::makeBody(BodyModel *body, const std::string &resource)
{
	BodyViewOI *view = new BodyViewOI( body, _screen );

	SoSeparator *bodySep = new SoSeparator();

	SoTransform *bodyTransform = new SoTransform();
	const Vect2f &vPos = body->getPosition();
	bodyTransform->translation.setValue( vPos[0], vPos[1], 0.0 );
	bodyTransform->scaleFactor.setValue( PUCK_RADIUS, PUCK_RADIUS, 1.0 );

	bodySep->addChild( bodyTransform );
	bodySep->addChild( new SoCylinder() );
	screen()->addChild( bodySep );

	view->setTransform( bodyTransform );

	_bodyViews.push_back( view );
	return view;
}

void ScreenDriverOI::fnKeyboardEventCB( void *userdata, SoEventCallback *eventCB )
{
	ScreenDriverOI* self = static_cast<ScreenDriverOI*>( userdata );
	if (self && eventCB ) {
		if (self->onKeyboardEvent( eventCB->getEvent() )) {
			eventCB->setHandled();
		}
	}
}

bool ScreenDriverOI::onKeyboardEvent( const SoEvent *event )
{
	if (!event) return false;

	if (SO_KEY_PRESS_EVENT(event, SoKeyboardEvent::Q)) {
		SoQt::exitMainLoop();
		return true;
	}
	return false;
}

void ScreenDriverOI::fnMouseMotionEventCB( void *userdata, SoEventCallback *eventCB )
{
	ScreenDriverOI* self = static_cast<ScreenDriverOI*>( userdata );
	if (self && eventCB ) {
		if (self->onMouseMotionEvent( eventCB->getEvent() )) {
			eventCB->setHandled();
		}
	}
}

bool ScreenDriverOI::onMouseMotionEvent( const SoEvent *event )
{
	if (!event) return false;

	Vect2f worldPos;
	const SbVec2s &pos = event->getPosition();
	
	screen()->screen_to_world( worldPos, Vect2i(pos[0], pos[1]) );

	mouseMoved()( worldPos );
	return true;
}


