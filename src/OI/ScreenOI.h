
#ifndef __METRIC_SCREEN_OI_H
#define __METRIC_SCREEN_OI_H 1

#include <vector>
#include <string>

#include <Inventor/Qt/SoQt.h>
#include <Inventor/Qt/SoQtRenderArea.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoSeparator.h>

#include "const.h"
#include "Screen.h"
#include "TableCamera.h"

namespace metric {


//
// Screen specialized for Open Inventor (Coin3D)
//
class ScreenOI : public Screen
{
public:
	ScreenOI(int argc, char *argv[]);
	virtual ~ScreenOI();

	virtual bool open( const std::string &windowTitle );
	virtual bool close();

	virtual unsigned int width() const;
	virtual unsigned int height() const;
	virtual Rect2f world() const;
	bool screen_to_world(Vect2f &dstWorld, const Vect2i &srcScreen) const;

	virtual void clear();

	void addChild( SoNode *child );

protected:
	QWidget *_window;
	SoSeparator *_root;
	//SoOrthographicCamera *_camera;
	TableCamera *_camera;
	SoMaterial *_redMaterial;
	SoQtRenderArea *_renderArea;
};

}

#endif

