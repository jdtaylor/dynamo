
#include <QWidget>

#include <Inventor/nodes/SoCylinder.h>
#include <Inventor/nodes/SoDirectionalLight.h>

#include "const.h"
#include "ScreenOI.h"


using namespace std;
using namespace metric;


ScreenOI::ScreenOI( int argc, char *argv[] ) :
	Screen(argc, argv)
{
	_window = SoQt::init( argv[0] );
	if (_window == NULL) {
		M_ERROR << "Failed to initialize SoQt. Aborting" << endl;
		exit(1);
	}
	_window->setMinimumSize( QSize(1024,1024) );

	// create sceme root
	_root = new SoSeparator(); 
	_root->ref();

	SoSeparator *camNode = new SoSeparator();
	// create camera
	_camera = new TableCamera(); //SoOrthographicCamera();
	camNode->addChild( _camera );
	_root->addChild(camNode);

	// light
	SoDirectionalLight *light = new SoDirectionalLight();
	light->direction = SbVec3f( 0.0, 0.0, -1.0 );
	_root->addChild(new SoDirectionalLight());

	// material
	_redMaterial = new SoMaterial();
	_redMaterial->diffuseColor.setValue(1.0, 0.0, 0.0);
	_root->addChild(_redMaterial);
}

ScreenOI::~ScreenOI()
{
	if (_renderArea) {
		// release memory
		delete _renderArea;
	}
	if (_root) {
		_root->unref();
	}
}

bool ScreenOI::open( const std::string &windowTitle )
{
	// cone
	//_root->addChild(new SoCylinder());

	// create window
	_renderArea = new SoQtRenderArea(_window);

	// This will cause the camera to zoom to the extents of 
	// the entire scene graph.. I instead do this in TableCamera.
	//_camera->viewAll(_root, _renderArea->getViewportRegion());

	// set up rendering window
	_renderArea->setSceneGraph(_root);
	_renderArea->setTitle(windowTitle.c_str());
	_renderArea->show();

	// show window
	SoQt::show(_window);

	return true;
}

bool ScreenOI::close()
{
	return true;
}

unsigned int ScreenOI::width() const
{
	return SCREEN_WIDTH;
}

unsigned int ScreenOI::height() const
{
	return SCREEN_HEIGHT;
}

Rect2f ScreenOI::world() const
{
	return Rect2f( Vect2f(0.0, 0.0), Vect2f(TABLE_WIDTH, TABLE_LENGTH) );
}

void ScreenOI::clear()
{
}

void ScreenOI::addChild( SoNode *child )
{
	if (_root) {
		_root->addChild( child );
	}
}

bool ScreenOI::screen_to_world(Vect2f &dstWorld, const Vect2i &srcScreen) const
{
	dstWorld.x() = (double)srcScreen.x() / width() * world().dx();
	dstWorld.y() = (double)srcScreen.y() / height() * world().dy();
}



