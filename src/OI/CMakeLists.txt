
FILE(GLOB HEADERS *.h)
FILE(GLOB SOURCES *.cpp *.c)

LIST(SORT HEADERS)
LIST(SORT SOURCES)

INCLUDE_DIRECTORIES(.)
INCLUDE_DIRECTORIES(${OPENINVENTOR_INCLUDE_DIR})

ADD_LIBRARY(dynamoOI ${HEADERS} ${SOURCES})

TARGET_LINK_LIBRARIES(dynamoOI dynamo)
