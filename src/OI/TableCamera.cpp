
#include "const.h"
#include "TableCamera.h"

using namespace metric;

TableCamera::TableCamera() :
	SoOrthographicCamera()
{
	float tableWidth = 4.0f / FEET_PER_METER;
	viewBoundingBox( SbBox3f( 0.0f, 0.0f, -1.0f, 
							  tableWidth, tableWidth, 1.0f ),
					 1.0f,   // aspect ratio
					 0.0f ); // slack (border multiple of)

	// look at center of table with positive Y-Axis "up"
	position = SbVec3f( tableWidth/2.0, tableWidth/2.0, -20.0 );
	pointAt( SbVec3f(tableWidth/2.0, tableWidth/2.0, 0.0f), SbVec3f(0.0, 1.0, 0.0) );
}

TableCamera::~TableCamera()
{
}


