
#ifndef __SCREEN_FACTORY_OI_H
#define __SCREEN_FACTORY_OI_H

#include "ScreenDriver.h"
#include "ScreenOI.h"

class SoEvent;
class SoEventCallback;

namespace metric {

class ScreenDriverOI : public ScreenDriver
{
public:
	ScreenDriverOI(int argc, char *argv[]);
	virtual ~ScreenDriverOI();

	virtual void exec();
	virtual bool loadResources();

	virtual ScreenOI* screen() { return dynamic_cast<ScreenOI*>(_screen); }

	virtual BodyView* makeBody(BodyModel *body, const std::string&);

protected:
	bool onKeyboardEvent( const SoEvent *event );
	bool onMouseMotionEvent( const SoEvent *event );

private:
	SoEventCallback *_keyboardEventCallback;
	static void fnKeyboardEventCB( void *userdata, SoEventCallback *eventCB );

	SoEventCallback *_mouseMotionEventCallback;
	static void fnMouseMotionEventCB( void *userdata, SoEventCallback *eventCB );
};

}

#endif

