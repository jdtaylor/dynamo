
#include <cassert>

#include <Inventor/nodes/SoTransform.h>

#include "log.h"
#include "Vect.h"
#include "BodyViewOI.h"


using namespace std;
using namespace metric;

BodyViewOI::BodyViewOI( BodyModel *body,
						Screen *screen ) :
	BodyView(body, screen),
	_transform(NULL)
{
	//Vect2f vDim = body->getExtents();
}

BodyViewOI::~BodyViewOI()
{
}

void BodyViewOI::positionChanged( Vect2f pos )
{
	if (_transform) {
	//	M_USER << "BodyViewOI Got position changed: " << pos << endl;
		_transform->translation.setValue( pos[0], pos[1], 0.0 );
	}
}

void BodyViewOI::setTransform( SoTransform *trans )
{
	_transform = trans;
	_transform->ref();
}

void BodyViewOI::draw()
{
	M_USER << "I'm in BodyViewOI::draw!" << endl;
	/*
	ScreenOI *scr = screen();
	if(!scr) return;

	// erase old body position by blitting background image over it
	BLIT(scr->background(), &_bounds, scr->surface(), &_bounds);
	SDL_Rect rectOld = _bounds;

	// get current body position and convert to screen coordinates
	Vect2f curPos = body()->getPosition();
	get_screen_rect(curPos, _bounds);

	switch(body()->getShape())
	{
	default:
	case BodyModel::BODY_SHAPE_CYLINDER:
		drawAsCylinder();
		break;
	case BodyModel::BODY_SHAPE_BOX:
		drawAsBox();
		break;
	}

	UPDATE_RECTS(scr->surface(), 1, &rectOld); // update erased 
	UPDATE_RECTS(scr->surface(), 1, &_bounds); // update newly drawn
	//UPDATE_RECTS(_screen, 0, NULL); // update newly drawn

	// invalidate because we just rendered it
	invalidate( false );
	*/
}

void BodyViewOI::drawAsCylinder()
{
	/*
	ScreenOI *scr = screen();
	if(!scr) return;

	switch(_drawingMode)
	{
		case DRAW_WIREFRAME:
			SDL_Rect center;
			center.x = _bounds.x + _extentsScreen.x();
			center.y = _bounds.y + _extentsScreen.y();
			//printf("%d %d %d\n", m_rect.x, m_rect.y, m_rect.w);
			Draw_Circle(scr->surface(), center.x, center.y, _extentsScreen.x(),
					SDL_MapRGB(scr->surface()->format, 255, 255, 255));

			SDL_Rect angle; // I negate angle because SDL is upside down compared to world
			angle.x = (Sint16)(_extentsScreen.x() * cos( -body()->getAngle() ));
			angle.y = (Sint16)(_extentsScreen.y() * sin( -body()->getAngle() ));
			if(angle.x >= 1) angle.x--; 
			if(angle.y >= 1) angle.y--;
			//	circleColor(_screen, m_rect.x, m_rect.y, m_rect.w, 0xFFFFFFFF);
			Draw_Line(scr->surface(), center.x, center.y, center.x + angle.x,
					center.y + angle.y, SDL_MapRGB(scr->surface()->format, 255, 255, 255));
			//				255, 255, 255, 255);
			break;
		case DRAW_SPRITE:
			// draw current position of body
			BLIT(_surface, NULL, scr->surface(), &_bounds);
			break;
	}
	*/
}

void BodyViewOI::drawAsBox()
{
	/*
	ScreenOI *scr = screen();
	if(!scr) return;

	switch(_drawingMode)
	{
		case DRAW_WIREFRAME:
			Draw_Rect(scr->surface(), _bounds.x, _bounds.y, _bounds.w, _bounds.h,
					SDL_MapRGB(scr->surface()->format, 255, 255, 255));
			break;
		case DRAW_SPRITE:
			// draw current position of body
			BLIT(_surface, NULL, scr->surface(), &_bounds);
			break;
	}
	*/
}

