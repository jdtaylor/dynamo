
#ifndef __METRIC_BODY_VIEW_OI_H
#define __METRIC_BODY_VIEW_OI_H 1

#include "Vect.h"

#include "BodyView.h"
#include "BodyModel.h"

#include "ScreenOI.h"


class SoTransform;

namespace metric {

class BodyViewOI : public BodyView
{
public:
	BodyViewOI( BodyModel *body,
			  Screen *screen );

	virtual ~BodyViewOI();

	virtual BodyModel* body() {
		return dynamic_cast<BodyModel*>( View::model() );
	}
	virtual ScreenOI* screen() {
		return dynamic_cast<ScreenOI*>( View::screen() ); 
	}

	virtual void draw();

	void correct_pos(Vect2i &pos);

	void setTransform( SoTransform *trans );

protected:
	SoTransform *_transform;

	virtual void positionChanged( Vect2f pos );

	void drawAsCylinder();
	void drawAsBox();
};

}

#endif

