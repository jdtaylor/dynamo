
#ifndef __METRIC_ENGINE_H
#define __METRIC_ENGINE_H 1

#include <list>
#include <map>

#include <sigc++/sigc++.h>
#include <ode/ode.h>

#include "Object.h"
#include "BodyModel.h"

namespace metric {

typedef unsigned long long microsec_t;

class Impulse;

class Engine : public Object
{
public:
	Engine( Rect2f world );
	virtual ~Engine();

	virtual void exec();
	virtual void step();
	virtual void quit() { _running = false; }
	virtual void setFramerate( double rate );
	virtual double getFramerate() const { return _desiredFramerate; }
	virtual double getFrameDuration() const {
		return _desiredFrameDuration / 1000000.0; // microsec -> seconds
	}
	virtual void startFrame();
	virtual void finishFrame();
	sigc::signal<void>& preStep() { return _preStep; }
	sigc::signal<void>& postStep() { return _postStep; }
	sigc::signal<void,BodyModel*,BodyModel*>& collision() { return _collision; }

	virtual bool registerBody( BodyModel *body );

	virtual Vect2f getBodyPosition( BodyModel *body ) const;
	virtual Vect2f getBodyVelocity( BodyModel *body ) const;

protected:
	microsec_t calcCurrentTime() const;
	void syncControlledBodies();
	void syncSimulatedBodies();

	Rect2f _world;
	const Rect2f world() const { return _world; }
	
	bool _running;

	// Signals:
	sigc::signal<void> _preStep;
	sigc::signal<void> _postStep;
	sigc::signal<void,BodyModel*,BodyModel*> _collision;

	// Engine state:
	std::list<BodyModel*> _bodies;
	std::list<Impulse> _impulses;
	double _desiredFramerate;
	double _actualFramerate;
	microsec_t _desiredFrameDuration;
	microsec_t _actualFrameDuration;
	microsec_t _startTime;
	microsec_t _finishTime;

	class odeBody {
	public:
		odeBody() {
			id = 0;
			model = NULL;
			//dMassSetZero( &mass );
		}

		dBodyID id;
		BodyModel* model;
		dMass mass;
		dGeomID geom;
		dJointID planeJoint;
	};

	odeBody& createPlane(double a, double b, double c, double d);
	odeBody& createBody();
	odeBody& createBox( Vect2f min, Vect2f max);

	//
	// ODE state:
	//
	typedef std::map<BodyModel*, odeBody*> BodyModelMap;
	BodyModelMap _bodyModelMap;

	typedef std::map<dBodyID, odeBody*> BodyIdMap;
	BodyIdMap _bodyIdMap;

	dWorldID _worldId;
	dSpaceID _spaceId;
	dJointGroupID _contactJointGroup;
	static void nearCollisionCallback( void *data, dGeomID o1, dGeomID o2 );
	void nearCollisionCallback( dGeomID o1, dGeomID o2 );
	void createTable();
	void alignBodyToZAxis( dBodyID bodyId );

private:
	Engine();
	Engine( const Engine & );
	Engine& operator=( const Engine & );
};

}

#endif

