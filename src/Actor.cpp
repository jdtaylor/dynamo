
#include "Actor.h"
#include "Engine.h"

using namespace metric;

Actor::Actor( Engine *engine, BodyModel *body ) :
	Object(),
	_engine( engine ),
	_body( body )
{
	assert(engine);
	assert(body);
}

Actor::~Actor()
{
}

void Actor::step()
{
}


