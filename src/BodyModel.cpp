
#include <cassert>

#include "BodyModel.h"

using namespace metric;

BodyModel::BodyModel( const std::string &name ) :
	Model(name),
	_class(BODY_CLASS_DYNAMIC),
	_shape(BODY_SHAPE_CYLINDER),
	_extents(1.0,1.0),
	_radius(1.0),
	_height(1.0),
	_mass(1.0),
	_pos(0.0, 0.0),
	_vel(0.0, 0.0),
	_angle(0.0),
	_angVel(0.0)
{
}

BodyModel::~BodyModel()
{
}

void BodyModel::collisionOccured( const BodyModel *otherBody )
//								  const Vect2f& pos, const Vect2f& norm )
{
	assert(otherBody);
	Vect2f otherPos = otherBody->getPosition();
//	M_USER << "Collision: " << pos << ", " << norm << std::endl;
}


void BodyModel::setPosition(const Vect2f& newPos)
{
	if (newPos != _pos) {
		_mutex.lock();
		_pos = newPos;
		_mutex.unlock();

		positionChanged().emit( _pos );
		invalidate( true ); // redraw
	}
}

Vect2f BodyModel::getPosition() const
{
	Vect2f curPos;

	_mutex.lock();
	curPos = _pos;
	_mutex.unlock();

	return curPos;
}

Vect2f BodyModel::stepPosition( double dTime ) const
{
	return getPosition() + getVelocity() * dTime;
}

void BodyModel::setVelocity( const Vect2f& vel )
{
	// commented this out when setMalletPosition was activated.  
	// TODO: uncomment this when we overide MalletBody::setPosition()
	// only simulated bodies can directly change their velocity.
	//assert(_simulated);
	_mutex.lock();
	_vel = vel;
	_mutex.unlock();

	velocityChanged().emit( _vel );
	invalidate( true );
}

Vect2f BodyModel::getVelocity() const
{
	Vect2f curVel;

	_mutex.lock();
	curVel = _vel;
	_mutex.unlock();

	return curVel;
}

void BodyModel::setAngle(double angle)
{
	_mutex.lock();
	_angle = angle;
	_mutex.unlock();

	invalidate( true ); // redraw
}

double BodyModel::getAngle() const
{
	double curAngle;

	_mutex.lock();
	curAngle = _angle;
	_mutex.unlock();

	return curAngle;
}

double BodyModel::stepAngle( double dTime ) const {
	return getAngle() + getAngularVelocity() * dTime;
}

double BodyModel::distance(const Vect2f &pos) const {
	return _pos.distance( pos );
}

Vect2f BodyModel::getLinearMomentum() const {
	return getVelocity() * getMass();
}

double BodyModel::getAngularMomentum() const {
	return getAngularVelocity() * getRotationalInertia();
}

void BodyModel::setAngularVelocity(double angVel)
{
	_mutex.lock();
	_angVel = angVel;
	_mutex.unlock();

	invalidate( true );
}

double BodyModel::getAngularVelocity() const
{
	double curAngVel;

	_mutex.lock();
	curAngVel = _angVel;
	_mutex.unlock();

	return curAngVel;
}

void BodyModel::setAngularMomentum( double angMomentum ) {
	setAngularVelocity( angMomentum / getRotationalInertia() );
	invalidate( true );
}

double BodyModel::getKineticEnergy() const {
	return getLinearKineticEnergy() + getAngularKineticEnergy();
}

double BodyModel::getLinearKineticEnergy() const {
	double vel = getVelocity().magnitude();
	return 0.5 * getMass() * vel*vel;
}

double BodyModel::getAngularKineticEnergy() const {
	double vel = getAngularVelocity();
	return 0.5 * getRotationalInertia() * vel*vel; // 1/2 * inertia * angular_velocity_squared
}

void BodyModel::setRadius(double radius) {
	_radius = radius;
	invalidate( true );
}

Vect2f BodyModel::getRadiusVec() const {
	return Vect2f(cos(_angle), sin(_angle)) * _radius;
}

void BodyModel::setMass(double mass) {
	_mass = mass;
	invalidate( true );
}

double BodyModel::getRotationalInertia() const {
	double radius = getRadius();
	return 0.5 * getMass() * radius * radius; // solid disk rotating like wheel
}

bool BodyModel::localToWorld( const Vect2f &local, Vect2f &world )
{
	world = _worldToLocal.inverse().transform( local );
	return true;
}

bool BodyModel::worldToLocal( const Vect2f &world, Vect2f &local )
{
	local = _worldToLocal.transform( world );
	return true;
}



