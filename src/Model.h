
#ifndef __METRIC_MODEL_H
#define __METRIC_MODEL_H 1

#include "Object.h"
#include "Streamable.h"

namespace metric {

class Model : public Object, public Streamable
{
public:
	Model(const std::string &name="");
	virtual ~Model();

	virtual bool invalid() const { return _invalid; }
	virtual void invalidate(bool b) { _invalid = b; }

protected:
	bool _invalid;
};

}

#endif

