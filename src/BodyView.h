
#ifndef __METRIC_BODY_VIEW_H
#define __METRIC_BODY_VIEW_H 1

#include "View.h"
#include "BodyModel.h"
#include "Color.h"

namespace metric {

class BodyView : public View
{
public:
	BodyView( BodyModel *body,
			  Screen *screen );
	virtual ~BodyView();

	virtual BodyModel* body() {
		return dynamic_cast<BodyModel*>( View::model() );
	}
	virtual Screen* screen() {
		return dynamic_cast<Screen*>( View::screen() ); 
	}

	virtual void draw() = 0;
	virtual void setVisible( bool visible ) = 0;
	virtual void setColor(const Color &c) { _fgColor = c; }

	enum {
		DRAW_WIREFRAME=0,
		DRAW_SOLID,
		DRAW_SPRITE,
		MAX_DRAWING_MODE
	};

	// Debatable whether these should be here, but it's convenient:
	void setDrawingMode( int drawingMode ) { _drawingMode = drawingMode; }
	int getDrawingMode() const { return _drawingMode; }

protected:
	virtual void positionChanged( Vect2f pos ); // slot

	Color _fgColor;
	int _drawingMode;
};

}

#endif


