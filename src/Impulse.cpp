
#include "Impulse.h"

using namespace metric;

void Impulse::step( double dTime )
{
	double dT = std::min(dTime, _duration);

	Vect2f acc = _force / _body->getMass();
	Vect2f dVel = acc * dT;

	_body->setVelocity( _body->getVelocity() + dVel );

	_duration -= dT;
}

