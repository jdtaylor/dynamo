
#include "MalletModel.h"
#include "const.h"

using namespace metric;

MalletModel::MalletModel() :
	ControlledBodyModel( "MalletModel" )
{
	setRadius( MALLET_RADIUS );
	setHeight( MALLET_HEIGHT );
	setMass( MALLET_MASS );
	setShape( BODY_SHAPE_CYLINDER );
	setExtents( Vect2f( MALLET_RADIUS, MALLET_RADIUS ) );
}

MalletModel::~MalletModel()
{
}

