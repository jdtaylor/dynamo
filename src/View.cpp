
/*
 * Has to do with screen resolution?
 * Application independent. (or generic)
 */
#include "View.h"

using namespace metric;

View::View( Model* model, Screen* screen ) :
	Object(),
	_model( model ),
	_screen( screen )
{
	assert(_model);
	assert(_screen);
}

View::~View()
{
}
