
#ifndef _MUTIL_VECT_H
#define _MUTIL_VECT_H

#include <iostream>
#include <cstring>
#include <cmath>
#include <cassert>

// TODO: take out
#include "log.h"
#include "Exception.h"

namespace metric
{

#define VECT_EPSILON 0.000001


//!
// Generic Vector
//
template <unsigned int Degree, class Type>
class Vect
{
public:
	typedef Type value_type;
	typedef Vect<Degree,Type> vect_type;

	Vect() {
		static_assert(Degree > 0, "Vect constructor requires Degree>0");
	}
	/*
	Vect(Type val) { 
		static_assert(Degree > 0, "Vect constructor requires Degree>0");
		for(unsigned int i=0; i < Degree; ++i)
			v[i] = val;
	}
	*/
	Vect(const Type &val) { 
		static_assert(Degree > 0, "Vect constructor requires Degree>0");
		for(unsigned int i=0; i < Degree; ++i)
			v[i] = val;
	}
	Vect(const Type *vect) {
		static_assert(Degree > 0, "Vect constructor requires Degree>0");
	 	memcpy(v, vect, sizeof(Type) * Degree);
	}
	Vect(const vect_type &vect) {
		static_assert(Degree > 0, "Vect constructor requires Degree>0");
		memcpy(v, vect.v, sizeof(Type) * Degree);
	}
	Vect(Type x, Type y) {
		static_assert(Degree > 0, "Vect constructor requires Degree>0");
		v[0] = x;
		if(Degree > 1) {
			v[1] = y;
		}
	}
	Vect(Type x, Type y, Type z) {
		static_assert(Degree > 0, "Vect constructor requires Degree>0");
		v[0] = x;
		if(Degree > 1) {
			v[1] = y;
			if(Degree > 2) {
				v[2] = z;
			}
		}
	}
	virtual ~Vect() {}

	size_t size() const { return static_cast<size_t>(Degree); }

	Type operator[](unsigned int i) const
	{
		if(i >= Degree)
			throw OobException();
		return v[i];
	}

	Type& operator[](unsigned int i)
	{
		if(i >= Degree)
			throw OobException();
		return v[i];
	}

	const Type& x() const { return v[0]; }
	Type& x() { return v[0]; }
	void x(const Type &val) { v[0] = val; }

	const Type& y() const {
		static_assert(Degree>1, "Vect::y only implemented for Degree>1");
		return v[1];
	}
	Type& y() {
		static_assert(Degree>1, "Vect::y only implemented for Degree>1");
		return v[1];
	}
	void y(const Type &val) {
		static_assert(Degree>1, "Vect::y only implemented for Degree>1");
		v[1] = val;
	}

	vect_type& operator=(const vect_type &rhs)
	{
		//memcpy(v, vect.v, sizeof(Type) * Degree);
		for(unsigned int i = 0; i < Degree; ++i)
			v[i] = rhs.v[i];
		return *this;
	}

	bool operator==(const vect_type &rhs) const
	{
		for(unsigned int i = 0; i < Degree; ++i)
			if(v[i] != rhs.v[i])
				return false;
		return true;
	}

	bool operator!=(const vect_type &vect) const
	{
		return !(*this == vect);
	}

	vect_type operator+(const vect_type& rhs) const {
		vect_type u;
		for(unsigned int i = 0; i < Degree; i++)
			u.v[i] = v[i] + rhs.v[i];
		return u;
	}

	vect_type operator+(const Type &val) const {
		vect_type u;
		for(unsigned int i = 0; i < Degree; i++)
			u.v[i] = v[i] + val;
		return u;
	}

	vect_type& operator+=(const vect_type& rhs) {
		for(unsigned int i = 0; i < Degree; i++)
			v[i] += rhs.v[i];
		return *this;
	}

	vect_type operator-(const vect_type& rhs) const {
		vect_type u;
		for(unsigned int i = 0; i < Degree; i++)
			u.v[i] = v[i] - rhs.v[i];
		return u;
	}

	vect_type operator-(const Type &val) const {
		vect_type u;
		for(unsigned int i = 0; i < Degree; i++)
			u.v[i] = v[i] - val;
		return u;
	}

	vect_type operator-() const {
		return vect_type(Type(0)) - *this;
	}

	vect_type operator*(const vect_type& rhs) const {
		vect_type u;
		for(unsigned int i = 0; i < Degree; i++)
			u.v[i] = v[i] * rhs.v[i];
		return u;
	}

	vect_type operator*(const Type &val) const {
		vect_type u;
		for(unsigned int i = 0; i < Degree; i++)
			u.v[i] = v[i] * val;
		return u;
	}

	vect_type& operator*=(const vect_type& rhs) {
		for(unsigned int i = 0; i < Degree; ++i)
			v[i] *= rhs.v[i];
		return *this;
	}

	vect_type& operator*=(const Type &val) {
		for(unsigned int i = 0; i < Degree; ++i)
			v[i] *= val;
		return *this;
	}

	vect_type operator/(const vect_type &rhs) const {
		vect_type u;
		for(unsigned int i = 0; i < Degree; ++i) {
			if (std::abs(rhs.v[i] - 0.0) < VECT_EPSILON) {
				throw DivZeroException("Vect::operator/ Division by zero");
			}
			u.v[i] = v[i] / rhs.v[i];
		}
		return u;
	}

	vect_type operator/(const Type &val) const { 
		if (std::abs(val - 0.0) < VECT_EPSILON) {
			throw DivZeroException("Vect::operator/ Division by zero");
		}
		vect_type u;
		for(unsigned int i = 0; i < Degree; ++i)
			u.v[i] = v[i] / val;
		return u;
	}

	// multiply and sum components
	Type sum() const {
		Type u = 0;
		for(unsigned int i = 0; i < Degree; ++i)
			u += v[i];
		return u;
	}

	//!
	// Distance from vect to us
	//
	Type distance(const vect_type &vect) const
	{
		Type sumsqr = 0;

		for(unsigned int i = 0; i < Degree; ++i) {
			Type d = v[i] - vect[i];
			sumsqr += d * d;
		}
		return std::sqrt(sumsqr);
	}

	Type magnitude() const {
		Type sumsqr = 0;
		for(unsigned int i = 0; i < Degree; ++i) {
			sumsqr += v[i] * v[i];
		}
		return std::sqrt(sumsqr);
	}

	vect_type normalize() const
	{
		Type mag = magnitude();

		if( mag < VECT_EPSILON ) {
			return vect_type(Type(0));
		}

		return vect_type(*this) / mag;
	}

	vect_type abs() const {
		vect_type u;
		for(unsigned int i = 0; i < Degree; ++i)
			u.v[i] = std::abs( v[i] );
		return u;
	}

	vect_type sign() const {
		vect_type u;
		for(unsigned int i = 0; i < Degree; ++i)
			u.v[i] = v[i] < 0.0 ? -1.0 : 1.0;
		return u;
	}

	std::ostream& operator>>(std::ostream& os) const
	{
		os << "[";
		for(unsigned int i = 0; i < Degree-1; ++i)
			os << v[i] << ",";
		os << v[Degree-1] << "]";
		return os;
	}

	// This is a retarded method.  It is only useful 
	// for calculating reflection of velocity vectors.
	vect_type reflect( const vect_type &norm ) const {
		vect_type u;
		for(unsigned int i = 0; i < Degree; ++i)
			u.v[i] = v[i] * (norm[i] == 0.0 ? 1.0 : -1.0);
		return u;
	}

	Type dot(const vect_type &rhs) const {
		static_assert(Degree==2, "Vect::dot only supports a Degree of 2");
		return v[0] * rhs.v[0] +
			   v[1] * rhs.v[1];
	}

	vect_type cross() const {
		static_assert(Degree==2, "Vect::cross only supports a Degree of 2");
		return vect_type( -v[1], v[0] );
		// [0,1]   -> [1,0]
		// [0,-1]  -> [-1,0]
		// [1,0]   -> [0,-1]
		// [-1,0]  -> [0,1]
	}

	// returns Z-component after cross-product with 'rhs'
	double crossZ(const vect_type &rhs) const {
		static_assert(Degree==2, "Vect::crossZ only supports a Degree of 2");
		return v[0] * rhs.v[1] -
			   v[1] * rhs.v[0];
	}

protected:
	Type v[Degree];
};

template <unsigned int Degree, class Type>
std::ostream& operator<<(std::ostream& os, const Vect<Degree,Type> &vec) { vec >> os; return os; }

typedef Vect<2,int> Vect2i;
typedef Vect<3,int> Vect3i;
typedef Vect<2,double> Vect2f;
typedef Vect<3,double> Vect3f;
typedef Vect<2,unsigned int> Vect2u;
typedef Vect<3,unsigned int> Vect3u;

}
#endif

