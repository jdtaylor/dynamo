
#include "log.h"

#include "ScreenSDL.h"


using namespace std;
using namespace metric;
/*
SDL_Surface* CreateSurface(Uint32 flags, int width, int height)
{
	SDL_Surface *surface;
	Uint32 rmask, gmask, bmask, amask;

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
	rmask = 0xff000000;
	gmask = 0x00ff0000;
	bmask = 0x0000ff00;
	amask = 0x000000ff;
#else
	rmask = 0x000000ff;
	gmask = 0x0000ff00;
	bmask = 0x00ff0000;
	amask = 0xff000000;
#endif

	surface = SDL_CreateRGBSurface(flags, width, height, 32,
			rmask, gmask, bmask, amask);
	if(surface == NULL) {
		VLOG_ERR("CreateSurface failed: %s\n", SDL_GetError());
		return NULL;
	}
	return surface;
}
*/

ScreenSDL::ScreenSDL( int argc, char *argv[] ) :
	Screen( argc, argv ),
	_surface(NULL),
	_background(NULL)
{
	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_NOPARACHUTE) < 0) {
		M_ERROR << "SDL_Init: " << SDL_GetError() << endl;
		exit(1);
	}
}

bool ScreenSDL::open( const std::string &windowTitle )
{
	const SDL_VideoInfo *vidInfo = NULL;
	Uint8 bpp = 24;
	Uint16 wd = SCREEN_WIDTH;
	Uint16 ht = SCREEN_HEIGHT;
	Uint32 flags = 0;

	vidInfo = SDL_GetVideoInfo();
	if(vidInfo <= 0) {
		M_ERROR << "SDL_GetVideoInfo: " << SDL_GetError() << endl;
		return false;
	}
	bpp = vidInfo->vfmt->BitsPerPixel;

	//flags |= SDL_HWPALETTE;
	//flags |= SDL_RESIZABLE;

	/*
	   if(vidInfo->hw_available)
	   flags |= SDL_HWSURFACE | SDL_DOUBLEBUF;
	   else
	   flags |= SDL_SWSURFACE;

	   if(vidInfo->blit_hw)
	   flags |= SDL_HWACCEL | SDL_DOUBLEBUF;
	 */

	//flags |= SDL_HWSURFACE | SDL_DOUBLEBUF;

	//
	// For fuck-sake, don't turn this flag on without stable closing
	// of the screen.. like in atexit() or something.
	// Even now that I have SDL_Quit on clean exit, It still messes 
	// up my dual screen.  To fix:
	//	xrandr --output DVI-0 --mode "1680x1050"
	//	xrandr --output DVI-0 --left-of DVI-1
	//
	//flags |= SDL_FULLSCREEN;


	//
	// This will actually open the window
	//
	_surface = SDL_SetVideoMode(wd, ht, bpp, flags);
	if(!_surface) {
		M_ERROR << "SDL_SetVideoMode: " << SDL_GetError() << endl;
		return false;
	}
	SDL_WM_SetCaption(windowTitle.c_str(), NULL);
	//SDL_WM_SetIcon(IMG_Load(PROG_ICON),NULL);

	//SDL_EnableKeyRepeat(1, 1);

	_screenBounds.x = 0;
	_screenBounds.y = 0;
	_screenBounds.w = SCREEN_WIDTH;
	_screenBounds.h = SCREEN_HEIGHT;

	printf( "bpp: %d\n", _surface->format->BitsPerPixel );
	//M_USER << string("bpp: ") << _surface->format->BitsPerPixel << endl;

	//SDL_PixelFormat fmt;
	_background = SDL_ConvertSurface(_surface, _surface->format, 0);
	//SDL_Surface *pTable = CreateSurface(SDL_SWSURFACE, 312, 600); // ratio = 1.92

	// Fill background with background color
	SDL_FillRect( background(), NULL, SDL_MapRGB(background()->format,
				  _bgColor.r(), _bgColor.g(), _bgColor.b()) );

	return true;
}

bool ScreenSDL::close()
{
	return true;
}

unsigned int ScreenSDL::width() const
{
	return SCREEN_WIDTH;
}

unsigned int ScreenSDL::height() const
{
	return SCREEN_HEIGHT;
}

Rect2f ScreenSDL::world() const
{
	return Rect2f( Vect2f(0.0, 0.0), Vect2f(TABLE_WIDTH, TABLE_LENGTH) );
}

void ScreenSDL::clear()
{
	SDL_Rect dstRect, srcRect;
	srcRect.x = 0;
	srcRect.y = 0;
	srcRect.w = width();
	srcRect.h = height();

	dstRect.x = 0;
	dstRect.y = 0;
	dstRect.w = 0; // w,h ignored by SDL 
	dstRect.h = 0; 

	// TODO: use macro:
	blit(background(), &srcRect, surface(), &dstRect);

	// Update entire screen
	SDL_UpdateRect(surface(), 0, 0, 0, 0);
}

bool ScreenSDL::world_to_screen(double worldX, double worldY, Uint16& screenX, Uint16& screenY)
{
	// These bounds checks are to prevent Uint16 overflow
	double x = ((worldX / world().dx()) * width()) + 0.5;
	if(x < 0.0) x = 0.0;
	if(x >= width()) x = width() - 1;
	screenX = static_cast<Uint16>( x );

	double y = ((1.0 - worldY / world().dy()) * height()) + 0.5;
	if(y < 0.0) y = 0.0;
	if(y >= height()) y = height() - 1;
	screenY = static_cast<Uint16>( y );
	return true;
}

bool ScreenSDL::world_to_screen(double worldX, double worldY, Sint16& screenX, Sint16& screenY)
{
	screenX = (Sint16)((worldX / world().dx()) * width()) + 0.5;
	screenY = (Sint16)((1.0 - worldY / world().dy()) * height()) + 0.5;
	return true;
}

bool ScreenSDL::screen_to_world(double& worldX, double& worldY, int screenX, int screenY)
{
	worldX = (double)screenX / width() * world().dx();
	worldY = (double)(1.0 - (double)screenY / height()) * world().dy();
	return true;
}

bool ScreenSDL::delta_world_to_screen(double dWorldX, double dWorldY, Uint16& dScreenX, Uint16& dScreenY)
{
	Sint16 screenUnity[2]; // position of 0,0 in screen coord
	Sint16 screenDelta[2]; // position of dx,dy in screen coord

	if (!world_to_screen( 0.0, 0.0, screenUnity[0], screenUnity[1] ))
		return false;
	if (!world_to_screen( dWorldX, dWorldY, screenDelta[0], screenDelta[1] ))
		return false;

	dScreenX = std::abs( screenDelta[0] - screenUnity[0] );
	dScreenY = std::abs( screenDelta[1] - screenUnity[1] );
	return true;
}

void ScreenSDL::clear_update_rects()
{
	_updateRects.clear();
}

bool ScreenSDL::point_in_rect( const SDL_Rect &rect, Sint16 x, Sint16 y )
{
	if( x > rect.x && x < rect.x + rect.w &&
		y > rect.y && y < rect.y + rect.h )
		return true;
	return false;
}

bool ScreenSDL::rects_intersect( const SDL_Rect &r1, const SDL_Rect &r2 )
{
	if( point_in_rect(r2, r1.x, r1.y) )
		return true;
	if( point_in_rect(r2, r1.x, r1.y + r1.w) )
		return true;
	if( point_in_rect(r2, r1.x + r1.w, r1.y) )
		return true;
	if( point_in_rect(r2, r1.x+r1.w, r1.y+r1.h) )
		return true;
	return false; // no intersection
}

// modify 'r1' in-place to be the union of r1 and r2
void ScreenSDL::rect_union( SDL_Rect &r1, const SDL_Rect &r2 )
{
	Sint16 x = std::max( r1.x+r1.w, r2.x+r2.w );
	Sint16 y = std::max( r1.y+r1.h, r2.y+r2.h );
	r1.x = std::min( r1.x, r2.x );
	r1.y = std::min( r1.y, r2.y );
	r1.w = x - r1.x;
	r1.h = y - r1.y;
}

void ScreenSDL::accumulate_rects(unsigned int n, SDL_Rect *rects)
{
	for(unsigned int i = 0; i < n; ++i) {
		const SDL_Rect &rect1 = rects[i];

		auto iRect = _updateRects.begin();
		for( ; iRect != _updateRects.end(); ++iRect)
		{
			SDL_Rect &rect2 = *iRect;

			if(rects_intersect( rect1, rect2 )) {
				rect_union( rect2, rect1 );
				break; // don't push new rect
			}
		}
		if( iRect != _updateRects.end() )
			continue;

		_updateRects.push_back( rects[i] );
	}
}

void ScreenSDL::update_rects(SDL_Surface *surface)
{
	/*
	unsigned int i = 0;
	auto iRect = _updateRects.begin();
	for( ; iRect != _updateRects.end(); ++iRect)
	{
		SDL_Rect &rect2 = *iRect;
		M_USER << "updating: " << i << " (" << rect2.x << "," << rect2.y;
		M_USER << ") -> (" << rect2.w << "," << rect2.h << ")" << std::endl;
		i++;
	}
	*/

	if( !_updateRects.empty() ) {
		SDL_Rect *rects = &_updateRects.front();
		SDL_UpdateRects(surface, _updateRects.size(), rects);
	}
	// update entire screen
	//SDL_UpdateRect(_surface, 0, 0, 0, 0);
}

void ScreenSDL::oob_error(const char *szFunction, unsigned int line, const char *szUser, SDL_Rect *rect)
{
	M_USER << "OOB at: " << szFunction << ":" << line << " \"" << szUser << "\" ";
	if(rect != NULL) {
		M_USER << " (" << rect->x << "," << rect->y << "," << rect->w << "," << rect->h << ")";
	}
	M_USER << endl;
}


template <typename T>
bool ScreenSDL::oob_adjust(T *vec2d, const SDL_Rect *bounds)
{
	bool bReturn = true;

	if(!bounds) bounds = &_screenBounds;
	if (!vec2d) return true;

	if(vec2d[0] < bounds->x) {
		vec2d[0] = bounds->x;
		bReturn = false;
	}
	if(vec2d[0] > bounds->x + bounds->w) {
		vec2d[0] = bounds->x + bounds->w;
		bReturn = false;
	}

	if(vec2d[1] < bounds->y) {
		vec2d[1] = bounds->y;
		bReturn = false;
	}
	if(vec2d[1] > bounds->y + bounds->h) {
		vec2d[1] = bounds->y + bounds->h;
		bReturn = false;
	}

	return bReturn;
}

bool ScreenSDL::oob_adjust(SDL_Rect *rect, const SDL_Rect *bounds)
{
	bool bReturn = true;

	if(!bounds) bounds = &_screenBounds;
	if(!rect) return true;

	if(rect->x < bounds->x) {
		rect->x = bounds->x;
		bReturn = false;
	}
	else if(rect->x + rect->w > bounds->x + bounds->w) {
		// don't change w,h as they only get set once at startup for sprites
		//rect->w = SCREEN_WIDTH-1 - rect->x;
		rect->x = (bounds->x + bounds->w) - rect->w;
		bReturn = false;
	}

	if(rect->y < bounds->y) {
		rect->y = bounds->y;
		bReturn = false;
	}
	else if(rect->y + rect->h > bounds->y + bounds->h) {
		//rect->h = SCREEN_HEIGHT-1 - rect->y;
		rect->y = (bounds->y + bounds->h) - rect->h;
		bReturn = false;
	}
	return bReturn;
}



