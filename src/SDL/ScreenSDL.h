
#ifndef __METRIC_SCREEN_SDL_H
#define __METRIC_SCREEN_SDL_H 1

#include <vector>
#include <string>

#include "SDL.h"

#include "const.h"
#include "Screen.h"

namespace metric {


class ScreenSDL : public Screen
{
public:
	ScreenSDL( int argc, char *argv[] );
	virtual ~ScreenSDL() {}

	virtual bool open( const std::string &windowTitle );
	virtual bool close();

	virtual unsigned int width() const;
	virtual unsigned int height() const;
	virtual Rect2f world() const;

	virtual void clear();

	SDL_Surface* surface() { return _surface; }
	SDL_Surface* background() { return _background; }

	bool world_to_screen(double worldX, double worldY, Uint16& screenX, Uint16& screenY);
	bool world_to_screen(double worldX, double worldY, Sint16& screenX, Sint16& screenY);
	bool screen_to_world(double& worldX, double& worldY, int screenX, int screenY);
	bool delta_world_to_screen(double dWorldX, double dWorldY, Uint16& dScreenX, Uint16& dScreenY);

	void clear_update_rects();
	bool point_in_rect( const SDL_Rect &rect, Sint16 x, Sint16 y );
	bool rects_intersect( const SDL_Rect &r1, const SDL_Rect &r2 );
	void rect_union( SDL_Rect &r1, const SDL_Rect &r2 );
	void accumulate_rects(unsigned int n, SDL_Rect *rects);
	void update_rects(SDL_Surface *surface);

	void oob_error(const char *szFunction, unsigned int line, const char *szUser, SDL_Rect *rect);
	template <typename T>
	bool oob_adjust(T *vec2d, const SDL_Rect *bounds=NULL);
	bool oob_adjust(SDL_Rect *rect, const SDL_Rect *bounds=NULL);

	void blit(SDL_Surface *src, SDL_Rect *srcRect, SDL_Surface *dst, SDL_Rect *dstRect,
			  const char *szFunction="<unknown>", unsigned int line=0)
	{
		if(!oob_adjust(srcRect)) oob_error(szFunction, line, "blit-src", srcRect);
		if(!oob_adjust(dstRect)) oob_error(szFunction, line, "blit-dst", dstRect);
		SDL_BlitSurface(src, srcRect, dst, dstRect);
	}

	void update_rects(SDL_Surface *screen, unsigned int n, SDL_Rect *rect,
			const char *szFunction="<unknown>", unsigned int line=0)
	{
		if(!oob_adjust(rect))  oob_error(szFunction, line, "update_rects", rect);
		if(rect->w != 0 && rect->h != 0)
			accumulate_rects(n, rect);
	}


protected:
	SDL_Surface *_surface;
	SDL_Surface *_background;
	SDL_Rect _screenBounds;
	std::vector<SDL_Rect> _updateRects;
};

}

#endif

