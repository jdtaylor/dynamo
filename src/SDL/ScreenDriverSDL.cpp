
#include "SDL.h"
#include "SDL_image.h"

#include "ScreenDriverSDL.h"
#include "ScreenSDL.h"
#include "BodyViewSDL.h"

using namespace metric;
using namespace std;


ScreenDriverSDL::ScreenDriverSDL(int argc, char *argv[]) :
	ScreenDriver(argc, argv)
{
	_screen = new ScreenSDL( argc, argv );
}

ScreenDriverSDL::~ScreenDriverSDL()
{
	delete _screen;

	SDL_Quit();
}

void ScreenDriverSDL::exec()
{
	while(42)
	{
		preFrame().emit();

		SDL_Event event;
		while(SDL_PollEvent(&event))
		{
			switch(event.type)
			{
				case SDL_QUIT:
					shutdown().emit();
					return;
				case SDL_KEYDOWN:
				{
					SDL_KeyboardEvent *key = (SDL_KeyboardEvent*)&event.key;
					if(key->type == SDL_KEYDOWN)
					{
						if(key->keysym.sym == SDLK_q) {
							shutdown().emit();
							return;
						} else if(key->keysym.sym == SDLK_r)
							restartSimulation().emit();
						//else if(key->keysym.sym == SDLK_m)
						//	setDrawingMode( (g_drawingMode+1) % MAX_DRAWING_MODE );
					}
					break;
				}
				case SDL_MOUSEMOTION:
				{
					Vect2f worldPos;

					SDL_MouseMotionEvent *motion = (SDL_MouseMotionEvent*)&event.motion;
					Vect2i pos( motion->x, motion->y );

					// Correct for mallet collisions with the wall
					BodyViewSDL *controlledView = dynamic_cast<BodyViewSDL*>( _controlledBodyView );
					if (controlledView) {
						controlledView->correct_pos( pos );
					}

					// Convert mouse pos to world coordinates. put coord in 'worldPos'
					screen()->screen_to_world(worldPos.x(), worldPos.y(), pos.x(), pos.y());

					// Emit mouse motion signal in world coordinates.  Screen coordinates 
					// should be kept within the Screen Driver.
					mouseMoved().emit( worldPos );
					break;
				}
				default:
					break;
			}
		}

		for_each( _bodyViews.begin(), _bodyViews.end(), [=](BodyView *view) {
			if(view->invalid()) {
				view->draw();
			}
		});

		// call SDL_UpdateRects for accumulated calls to UPDATE_RECTS
		screen()->update_rects( screen()->surface() );
		screen()->clear_update_rects();

		postFrame().emit();
	}
}

bool ScreenDriverSDL::loadResources()
{
	SDL_Surface *image;

	if( !(image = IMG_Load("../resources/puck57.png")) ) {
		M_ERROR << "Failed to load puck image" << endl;
		return false;
	}
	_resources["puck_red"] = image;

	if( ! (image = IMG_Load("../resources/red_mallet70.png"))) {
		M_ERROR << "Failed to load mallet image" << endl;
		return false;
	}
	_resources["mallet_white"] = image;

	// TODO: .. ?  This isn't even an image of a wall
	if( ! (image = IMG_Load("../resources/puck57.png"))) {
		M_ERROR << "Failed to load wall image" << endl;
		return false;
	}
	_resources["wall"] = image;


	return true;
}

BodyView* ScreenDriverSDL::makeBody(BodyModel *body, const std::string &resource)
{
	BodyViewSDL *view = new BodyViewSDL( body, _screen );

	SDL_Surface *image = getResource( resource );
	if (image) {
		view->setSurface( image );
	}

	_bodyViews.push_back( view );

	return view;
}

SDL_Surface* ScreenDriverSDL::getResource(const std::string &resource)
{
	ResourceMap::iterator it = _resources.find( resource );
	if(it == _resources.end()) {
		return NULL;
	}
	return it->second;
}


