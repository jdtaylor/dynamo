
#ifndef __SCREEN_FACTORY_SDL_H
#define __SCREEN_FACTORY_SDL_H

#include <map>

#include "SDL.h"

#include "ScreenDriver.h"
#include "ScreenSDL.h"

namespace metric {

class ScreenDriverSDL : public ScreenDriver
{
public:
	ScreenDriverSDL(int argc, char *argv[]);
	virtual ~ScreenDriverSDL();

	virtual void exec();

	virtual bool loadResources();

	virtual ScreenSDL* screen() { return dynamic_cast<ScreenSDL*>( _screen ); }

	virtual BodyView* makeBody(BodyModel *body, const std::string &resource);

protected:
	SDL_Surface* getResource(const std::string &resource);

	typedef std::map<std::string, SDL_Surface*> ResourceMap;
	ResourceMap _resources;
};

}

#endif

