
#ifndef __METRIC_BODY_VIEW_SDL_H
#define __METRIC_BODY_VIEW_SDL_H 1

#include "Vect.h"

#include "BodyView.h"
#include "BodyModel.h"

#include "ScreenSDL.h"


namespace metric {


class BodyViewSDL : public BodyView
{
public:
	BodyViewSDL( BodyModel *body,
			  Screen *screen );

	virtual ~BodyViewSDL();

	virtual BodyModel* body() {
		return dynamic_cast<BodyModel*>( View::model() );
	}
	virtual ScreenSDL* screen() {
		return dynamic_cast<ScreenSDL*>( View::screen() ); 
	}

	virtual void draw();

	virtual void setVisible(bool visible) {
		_visible = visible;
		invalidate(true);
	}

	virtual void setSurface( SDL_Surface *surface ) { _surface = surface; }
	virtual SDL_Surface* getSurface() { return _surface; }

	void correct_pos(Vect2i &pos);
	void get_screen_rect(Vect2f vPos, SDL_Rect &dstRect);

protected:
	bool _visible;
	SDL_Surface *_surface;
	SDL_Rect _bounds;
	Vect2f _extentsScreen; // half-dimensions of body in screen coordinates
	Vect2f _maxVel; // for rendering velocity magnitudes

	void drawAsCylinder();
	void drawAsBox();
};

}

#endif

