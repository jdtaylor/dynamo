
#include <cassert>

#include "SDL.h"
#include "SDL_image.h"
//#include "SDL_draw.h"
#include "SDL_gfxPrimitives.h"

#include "log.h"
#include "Vect.h"
#include "BodyViewSDL.h"

#define BLIT(src,srcrect,dst,dstrect) \
	screen()->blit((src),(srcrect),(dst),(dstrect),__FUNCTION__,__LINE__)

#define UPDATE_RECTS(scr, n, rect) \
	screen()->update_rects((scr),(n),(rect),__FUNCTION__,__LINE__)

using namespace std;
using namespace metric;

BodyViewSDL::BodyViewSDL( BodyModel *body,
					Screen *screen ) :
	BodyView(body, screen),
	_visible(true),
	_surface(NULL),
	_extentsScreen( 0.0, 0.0 )
{
	Uint16 width, height;
	Vect2f vDim = body->getExtents();
	BodyViewSDL::screen()->delta_world_to_screen(vDim[0], vDim[1], width, height);
	_extentsScreen[0] = width;
	_extentsScreen[1] = height;

	get_screen_rect( body->getPosition(), _bounds );

	M_USER << "Created body: (" << _bounds.x << "," << _bounds.y << ")" << std::endl;
	M_USER << "              (" << _bounds.w << "," << _bounds.h << ")" << std::endl;
}

BodyViewSDL::~BodyViewSDL()
{
}

// Don't allow center 'pos' to pass within the body's screen extents
// of the side of the window.
void BodyViewSDL::correct_pos(Vect2i &pos)
{
	if(pos.x() - _extentsScreen.x() < 0)
		pos.x() = _extentsScreen.x();
	if(pos.x() + _extentsScreen.x() > screen()->width()-4)
		pos.x() = screen()->width() - _extentsScreen.x() - 4;

	if(pos.y() - _extentsScreen.y() < 0)
		pos.y() = _extentsScreen.y();
	if(pos.y() + _extentsScreen.y() > screen()->height()-4)
		pos.y() = screen()->height() - _extentsScreen.y() - 4;
}

void BodyViewSDL::get_screen_rect(Vect2f vPos, SDL_Rect &dstRect)
{
	// convert puck radius into width and height in screen pixels
	screen()->world_to_screen(vPos.x(), vPos.y(), dstRect.x, dstRect.y);
	dstRect.x -= _extentsScreen.x();
	dstRect.y -= _extentsScreen.y();
	dstRect.w = _extentsScreen.x() * 2.0;
	dstRect.h = _extentsScreen.y() * 2.0;
	//dstRect.h = dstRect.w; // IMPORTANT: see create_mallet above (why was this important?)
	screen()->oob_adjust(&dstRect);
}

void BodyViewSDL::draw()
{
	ScreenSDL *scr = screen();
	if(!scr) return;

	// erase old body position by blitting background image over it
	BLIT(scr->background(), &_bounds, scr->surface(), &_bounds);
	SDL_Rect rectOld = _bounds;

	UPDATE_RECTS(scr->surface(), 1, &rectOld); // update erased 

	if(_visible)
	{
		// get current body position and convert to screen coordinates
		Vect2f curPos = body()->getPosition();
		get_screen_rect(curPos, _bounds);

		switch(body()->getShape())
		{
			default:
			case BodyModel::BODY_SHAPE_CYLINDER:
				drawAsCylinder();
				break;
			case BodyModel::BODY_SHAPE_BOX:
				drawAsBox();
				break;
		}

		UPDATE_RECTS(scr->surface(), 1, &_bounds); // update newly drawn
		//UPDATE_RECTS(_screen, 0, NULL); // update newly drawn
	}

	// invalidate because we just rendered it
	invalidate( false );
}

void BodyViewSDL::drawAsCylinder()
{
	ScreenSDL *scr = screen();
	if(!scr) return;

	switch(getDrawingMode())
	{
		case BodyView::DRAW_WIREFRAME:
			SDL_Rect center;
			center.x = _bounds.x + _extentsScreen.x();
			center.y = _bounds.y + _extentsScreen.y();
			//printf("%d %d %d\n", m_rect.x, m_rect.y, m_rect.w);
			circleColor(scr->surface(), center.x, center.y, _extentsScreen.x()-1,
						SDL_MapRGB(BodyViewSDL::screen()->surface()->format, _fgColor.r(), _fgColor.g(), _fgColor.b()));

			SDL_Rect angle; // I negate angle because SDL is upside down compared to world
			angle.x = (Sint16)(_extentsScreen.x() * cos( -body()->getAngle() ));
			angle.y = (Sint16)(_extentsScreen.y() * sin( -body()->getAngle() ));
			if(angle.x >= 1) angle.x--; 
			if(angle.y >= 1) angle.y--;
			//	circleColor(_screen, m_rect.x, m_rect.y, m_rect.w, 0xFFFFFFFF);
			lineColor(scr->surface(), center.x, center.y, center.x + angle.x,
					center.y + angle.y,
						SDL_MapRGB(BodyViewSDL::screen()->surface()->format, _fgColor.r(), _fgColor.g(), _fgColor.b()));
			break;
		case BodyView::DRAW_SPRITE:
			// draw current position of body
			BLIT(_surface, NULL, scr->surface(), &_bounds);
			break;
	}

}

void BodyViewSDL::drawAsBox()
{
	ScreenSDL *scr = screen();
	if(!scr) return;

	M_USER << "Drawing body: (" << _bounds.x << "," << _bounds.y << ")" << std::endl;
	M_USER << "              (" << _bounds.x + _bounds.w - 1 << "," << _bounds.y + _bounds.h - 1 << ")" << std::endl;

	switch(getDrawingMode())
	{
		case BodyView::DRAW_WIREFRAME:
			rectangleColor(scr->surface(), _bounds.x, _bounds.y,
						   _bounds.x + _bounds.w - 1,
						   _bounds.x + _bounds.h - 1, 
						SDL_MapRGB(BodyViewSDL::screen()->surface()->format, _fgColor.r(), _fgColor.g(), _fgColor.b()));
			break;
		case BodyView::DRAW_SOLID:
			boxColor(scr->surface(), _bounds.x, _bounds.y,
			 	     _bounds.x + _bounds.w - 2,//1,
					 _bounds.x + _bounds.h - 2,//1, 
						SDL_MapRGB(BodyViewSDL::screen()->surface()->format, _fgColor.r(), _fgColor.g(), _fgColor.b()));
			M_USER << "WIth color: " << SDL_MapRGB(BodyViewSDL::screen()->surface()->format, _fgColor.r(), _fgColor.g(), _fgColor.b()) << std::endl;
			break;
		case BodyView::DRAW_SPRITE:
			// draw current position of body
			BLIT(_surface, NULL, scr->surface(), &_bounds);
			break;
	}
}

