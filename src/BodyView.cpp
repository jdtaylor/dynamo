
#include "log.h"
#include "BodyView.h"

using namespace metric;

BodyView::BodyView( BodyModel *body, Screen *screen ) :
	View( body, screen ),
	_drawingMode( DRAW_WIREFRAME )
{
	body->positionChanged().connect( sigc::mem_fun(this, &BodyView::positionChanged) );
}

BodyView::~BodyView()
{
}

void BodyView::positionChanged( Vect2f pos )
{
}

