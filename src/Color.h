
#ifndef __METRIC_COLOR
#define __METRIC_COLOR

namespace metric
{

class Color
{
public:
	Color() : _r(255), _g(255), _b(255), _a(255) {}
	Color(unsigned char r, unsigned char g, unsigned char b) :
		_r(r), _g(g), _b(b), _a(255) {}
	Color(unsigned char r, unsigned char g, unsigned char b, unsigned char a) :
		_r(r), _g(g), _b(b), _a(a) {}

	unsigned char r() const { return _r; }
	unsigned char g() const { return _g; }
	unsigned char b() const { return _b; }
	unsigned char a() const { return _a; }

protected:
	unsigned char _r;
	unsigned char _g;
	unsigned char _b;
	unsigned char _a;
};

} // namespace metric

#endif
