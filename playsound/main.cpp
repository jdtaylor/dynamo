
#include <unistd.h>
#include <iostream>

#include "log.h"
#include "Sound.h"

using namespace std;
using namespace metric;


int main(int argc, char *argv[])
{
	if(!Sound::init()) {
		M_ERROR << "Failed to open audio." << endl;
		exit(1);
	}

	string wavPath = "/home/yottahz/src/dynamo/sounds/AirHockeyPuck1.wav";
	if(!Sound::load("puck1", wavPath)) {
		M_ERROR << "Failed to open audio file:" << wavPath << endl;
		exit(1);
	}

	unsigned int duration = static_cast<unsigned int>(Sound::duration("puck1") + 1);

	Sound::play("puck1");

	sleep(duration);

	Sound::destroy();
	return 0;
}

