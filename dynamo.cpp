
#include <unistd.h>
#include <iostream>
#include <cassert>
#include <list>
#include <algorithm>
#include <thread>

#include "const.h"
#include "log.h"
#include "Vect.h"

#include "Sound.h"
#include "Engine.h"
#include "MalletModel.h"
#include "PuckModel.h"
#include "StaticBodyModel.h"

#include "OSG/ScreenDriverOSG.h"
//#include "SDL/ScreenDriverSDL.h"
//#include "OI/ScreenDriverOI.h"

using namespace std;
using namespace metric;


class DynamoLog : public LogCallback
{
public:
	virtual ~DynamoLog() {}
	virtual int operator() (LogType type, const std::string& msg)
	{
		std::string str = msg;

		if(str[str.length()-1] == '\n')
			str.resize(str.length()-1); // strip trailing newline

		//if(type == LOG_TYPE_DUMP)
		//	return 1;
		return LogCallback::operator()(type, msg);
	}
};

void odeErrorHandler(int errnum, const char *msg, va_list ap)
{
	char msgbuf[1024];
	vsnprintf(msgbuf, sizeof(msgbuf), msg, ap);
	M_ERROR << "ODE Error: " << msgbuf << endl;
}

void odeMessageHandler(int errnum, const char *msg, va_list ap)
{
	char msgbuf[1024];
	vsnprintf(msgbuf, sizeof(msgbuf), msg, ap);
	M_USER << "ODE Message: " << msgbuf << endl;
}

#define WALL_DEPTH 0.01
#define GOAL_DEPTH 0.04

Engine *g_engine;

ScreenDriver *g_driver;
Screen *g_screen;
MalletModel *g_malletBody;
BodyView *g_malletView;
int g_drawingMode = 0;


bool createMallet( Engine *engine, ScreenDriver *factory )
{
	MalletModel *malletBody = new MalletModel();

	Vect2f malletPos( g_screen->world().dx() / 2, g_screen->world().dy() / 4 );
	malletBody->setPosition(malletPos);
	malletBody->setControlPosition(malletPos);
	//malletBody->setVelocity( Vect2f( 0.0003, 0.1 ));

	g_malletBody = malletBody;

	engine->registerBody( malletBody );

	BodyView *view = factory->makeBody( malletBody, "mallet_white" );

	view->setVisible( false ); // don't render mallet (too much lag)

	g_malletView = view;

	return true;
}

/*
bool createSingleTarget( Engine *engine, ScreenDriver *factory)
{
	double targetWidth = 0.4;
	double targetHeight = 0.1;
	double wallWidth = (g_screen->world().dx() - goalWidth) / 2.0;

	double tableWidth = g_screen->world().dx();
	double tableLength = g_screen->world().dy();

	Vect2f vDim(wallWidth, GOAL_DEPTH);

	if(!createWall(engine, factory, Vect2f(WALL_DEPTH, tableLength - GOAL_DEPTH - WALL_DEPTH), vDim)) return false;

	StaticBodyModel *body = new StaticBodyModel( engine );

	Vect2f vCenter = vMin + vDim / 2.0;
	body->setPosition( vCenter );
	body->setExtents( vDim / 2.0 );
	body->setShape( BodyModel::BODY_SHAPE_BOX );

	engine->registerBody( body );

	factory->makeBody( body, "wall" );
}
*/

bool createPuck( Engine *engine, ScreenDriver *factory )
{
	PuckModel *puckBody = new PuckModel();

	Vect2f puckPos( g_screen->world().dx() / 2, g_screen->world().dy() / 2 );
	puckBody->setPosition( puckPos );
	//puckBody->setAngularVelocity( 2 * 3.14159 );
	//puckBody->setVelocity( Vect2f( 1.0, 3.0 ));

	engine->registerBody( puckBody );

	factory->makeBody( puckBody, "puck_red" );

	return true;
}

bool createWall( Engine *engine, ScreenDriver *factory, Vect2f vMin, Vect2f vDim )
{
	StaticBodyModel *body = new StaticBodyModel();

	Vect2f vCenter = vMin + vDim / 2.0;
	body->setPosition( vCenter );
	body->setExtents( vDim / 2.0 );
	body->setShape( BodyModel::BODY_SHAPE_BOX );

	engine->registerBody( body );

	factory->makeBody( body, "wall" );

	return true;
}

bool createGoals( Engine *engine, ScreenDriver *factory )
{
	double goalWidth = 0.4;
	double wallWidth = (g_screen->world().dx() - goalWidth) / 2.0;

	double tableWidth = g_screen->world().dx();
	double tableLength = g_screen->world().dy();

	Vect2f vDim(wallWidth, GOAL_DEPTH);

	if(!createWall(engine, factory, Vect2f(WALL_DEPTH, tableLength - GOAL_DEPTH - WALL_DEPTH), vDim)) return false;
	if(!createWall(engine, factory, Vect2f(tableWidth - wallWidth - WALL_DEPTH, tableLength - GOAL_DEPTH - WALL_DEPTH), vDim )) return false;
	if(!createWall(engine, factory, Vect2f(WALL_DEPTH, WALL_DEPTH), vDim)) return false;
	if(!createWall(engine, factory, Vect2f(tableWidth - wallWidth - WALL_DEPTH, WALL_DEPTH), vDim)) return false;

	return true;
}

bool createTable( Engine *engine, ScreenDriver *factory )
{
	if(!createGoals(engine, factory)) return false;

	double tableWidth = g_screen->world().dx();
	double tableLength = g_screen->world().dy();
	double wallDepth = WALL_DEPTH;

	// Left and right walls:
	Vect2f vDim( wallDepth, tableLength );
	if(!createWall(engine, factory, Vect2f(0, 0), vDim )) return false;
	if(!createWall(engine, factory, Vect2f(tableWidth - wallDepth, 0), vDim))
		return false;

	// Top and bottom:
	vDim = Vect2f( tableWidth, wallDepth );
	if(!createWall(engine, factory, Vect2f(0, 0), vDim )) return false;
	if(!createWall(engine, factory, Vect2f(0, tableLength - wallDepth), vDim))
		return false;
	return true;
}

void restart_simulation()
{
	M_USER << "Restarting Simulation" << endl;

	delete g_engine;
	g_engine = new Engine( g_screen->world() );

	// TODO delete all views and models.  reconstruct everything
	//
	if (!createPuck( g_engine, g_driver )) {
		M_ERROR << "Failed to recreate puck" << endl;
		return;
	}

	g_engine->registerBody( g_malletBody );

	g_screen->clear();
}

void mouseMoved(const Vect2f &worldPos)
{
	//M_USER << "Mouse moved: " << worldPos << endl;
	g_malletBody->setControlPosition( worldPos );
	//g_malletBody->setPosition( worldPos );
}

void preFrame()
{
}

void postFrame()
{
	// Check for external (non-mouse) mallet motion:
	//pollMultiTouch();
	usleep( 10000 ); // At most 100 Hz
}


void shutdown()
{
	g_engine->quit();
	//exit(0);
}

int main(int argc, char *argv[])
{
	Log::Instance().setCallback( new DynamoLog() );

	dSetErrorHandler( odeErrorHandler );
	dSetDebugHandler( odeMessageHandler );
	dSetMessageHandler( odeMessageHandler );

	if(!Sound::init()) {
		M_ERROR << "Failed to open audio." << endl;
		exit(1);
	}
	string wavPath = "/home/yottahz/src/dynamo/sounds/AirHockeyPuck1.wav";
	if(!Sound::load("puck1", wavPath)) {
		M_ERROR << "Failed to open audio file:" << wavPath << endl;
		exit(1);
	}

	g_driver = new ScreenDriverOSG(argc, argv);
	//g_driver = new ScreenDriverSDL(argc, argv);
	//g_driver = new ScreenDriverOI(argc, argv);

	// callback from graphics driver to here:
	g_driver->preFrame().connect( sigc::ptr_fun(&preFrame) );
	g_driver->postFrame().connect( sigc::ptr_fun(&postFrame) );
	g_driver->mouseMoved().connect( sigc::ptr_fun(&mouseMoved) );
	g_driver->restartSimulation().connect( sigc::ptr_fun(&restart_simulation) );
	g_driver->shutdown().connect( sigc::ptr_fun(&shutdown) );

	g_screen = g_driver->screen();
	if (!g_screen) {
		M_ERROR << "Failed to open screen." << endl;
		exit(1);
	}
	// TODO Setting background only works before opening screen
	g_screen->setBgColor(Color(49, 61, 170)); 
	g_screen->open(APP_TITLE);

	g_engine = new Engine( g_screen->world() );
	g_engine->setFramerate( 240 ); // works fine

	if( !createTable( g_engine, g_driver )) {
		M_ERROR << "Failed to create table" << endl;
		shutdown();
	}

	if( !createMallet( g_engine, g_driver )) {
		M_ERROR << "Failed to create mallet" << endl;
		shutdown();
	}

	if( !createPuck( g_engine, g_driver )) {
		M_ERROR << "Failed to create puck" << endl;
		shutdown();
	}

	// Set which body is controlled by mouse position:
	g_driver->setControlledBodyView( g_malletView );

	g_screen->clear();

	// Spawn thread to execute the physics engine 
	std::thread engineThread([=]() { 
		g_engine->exec();
	});

	// Use current thread to execute the screen driver. 
	// Qt will refuse to run in anything but the main thread.
	g_driver->exec();

	// Wait for physics engine to close:
	g_engine->quit();
	engineThread.join();

	//exitMultiTouch();

	delete g_engine;
	delete g_driver;

	Sound::destroy();
	return 0;
}

