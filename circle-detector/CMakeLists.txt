
#FIND_PACKAGE( OpenCV REQUIRED )

ADD_EXECUTABLE( circle-detect.exe main.cpp )
TARGET_LINK_LIBRARIES( circle-detect.exe ${OpenCV_LIBS} )

ADD_EXECUTABLE( webcam-cap.exe webcam-cap.cpp )
TARGET_LINK_LIBRARIES( webcam-cap.exe ${OpenCV_LIBS} )
