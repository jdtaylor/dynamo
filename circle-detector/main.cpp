
#include <stdio.h>
#include <cv.h>
#include <highgui.h>
#include <math.h>

int main(int argc, char** argv)
{
    IplImage* img = cvLoadImage("circle.jpg", 1);
    IplImage* gray = cvCreateImage(cvGetSize(img), 8, 1);
    CvMemStorage* storage = cvCreateMemStorage(0);
    cvCvtColor(img, gray, CV_BGR2GRAY);
    cvSmooth(gray, gray, CV_GAUSSIAN, 9, 9);
    CvSeq* circles = cvHoughCircles(gray, storage,
        CV_HOUGH_GRADIENT, // only choice 
        1, // 2 == accumulator is half resolution of image
        112, // min distance between centers of circles
        100, // high threshold
        10, // smaller = more circles detected
        45, // min radius
        49 // max radius
        );
    int i;
   
    for (i = 0; i < circles->total; i++)
    {   
         float* p = (float*)cvGetSeqElem( circles, i );
        printf("Found circle: (%.2f,%.2f,%.2f)\n", p[0], p[1], p[2]);
         cvCircle( img, cvPoint(cvRound(p[0]),cvRound(p[1])),
             3, CV_RGB(0,255,0), -1, 8, 0 );
         cvCircle( img, cvPoint(cvRound(p[0]),cvRound(p[1])),
             cvRound(p[2]), CV_RGB(255,0,0), 3, 8, 0 );
    }
    cvNamedWindow( "circles", 1 );
    cvShowImage( "circles", img );
    cvWaitKey(0);
   
    return 0;
}
