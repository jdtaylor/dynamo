#include <stdio.h>                                                                                     
#include <iostream>                                                                                    
#include <sys/time.h>
#include "cv.h"                                                                                        
#include "highgui.h"                                                                                   
using namespace std;

unsigned long long get_current_time()
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return (unsigned long long)tv.tv_sec * 1000000 + tv.tv_usec;
}

void find_circle( IplImage* img, IplImage *gray, CvMemStorage *storage )
{
	cvClearMemStorage( storage );

    cvCvtColor(img, gray, CV_BGR2GRAY);
    cvSmooth(gray, gray, CV_GAUSSIAN, 9, 9);
    CvSeq* circles = cvHoughCircles(gray, storage,
        CV_HOUGH_GRADIENT, // only choice 
        2, // 2 == accumulator is half resolution of image
        112, // min distance between centers of circles
        100, // high threshold
        10, // smaller = more circles detected
        45, // min radius
        49 // max radius
        );
   
    for (int i = 0; i < circles->total; i++)
    {   
		float* p = (float*)cvGetSeqElem( circles, i );
		printf("%d: (%8.2f,\t%8.2f,\t%8.2f)\n", i, p[0], p[1], p[2]);
    }
}

int main(int argc, char** argv)
{
	unsigned long long current_frame = 0;
	unsigned long long current_time = get_current_time();
	unsigned long long previous_time = current_time;
	double dtime = 0.0;
	double dtime_total = 0.0;
	char *buf;
	IplImage *frame = 0;
	IplImage *gray = 0;
	CvCapture* capture = cvCreateCameraCapture(-1);
	if (!capture) {
		std::cerr << "Failed to open camera" << std::endl;
	}
	cvSetCaptureProperty( capture, CV_CAP_PROP_FRAME_WIDTH, 320);
	cvSetCaptureProperty( capture, CV_CAP_PROP_FRAME_HEIGHT, 240);
	cvSetCaptureProperty( capture, CV_CAP_PROP_FPS, 125);

    CvMemStorage* storage = cvCreateMemStorage(0);

	while (true)
	{
		frame = cvQueryFrame(capture);
		if (!frame)
			break;

		if (!gray) {
			gray = cvCreateImage(cvGetSize(frame), 8, 1);
			if (!gray)
				break;
		}

		find_circle( frame, gray, storage );

		current_frame++;
		current_time = get_current_time();
		dtime = double(current_time - previous_time) / 1000000.0; // in seconds
		dtime_total += dtime;
		previous_time = current_time;

		if(current_frame % 100 == 0) {
			printf("fps: %f\n", 100.0f / dtime_total);
			dtime_total = 0.0;
		}

		if(current_frame == 1) {
			// save first image to disk
			if(asprintf(&buf, "reference_frame.jpg") > 0) {
				//	cvShowImage("1", frame);
				cvSaveImage(buf, frame);
				//	cvWaitKey(10);
				free(buf);
			}
		}
	}                                                                                                  
	return 0;
}                                                                                                      

/*                                                                                                     
#include "cv.h"                                                                                        
#include "highgui.h"                                                                                   
#include <stdio.h>                                                                                     
// A Simple Camera Capture Framework                                                                   
int main() {                                                                                           
    CvCapture* capture = cvCaptureFromCAM( CV_CAP_ANY );
    if ( !capture ) {                                                                                  
        fprintf( stderr, "ERROR: capture is NULL \n" );
        getchar();
        return -1;
    }                                                                                                  
    printf( " opened capture device!\n " );
    // Create a window in which the captured images will be presented                                  
    cvNamedWindow( "mywindow", CV_WINDOW_AUTOSIZE );
    // Show the image captured from the camera in the window and repeat                                
    while ( 1 ) {                                                                                      
        // Get one frame                                                                               
        IplImage* frame = cvQueryFrame( capture );
        if ( !frame ) {                                                                                
            fprintf( stderr, "ERROR: frame is null...\n" );
            getchar();
            break;
        }                                                                                              
        cvShowImage( "mywindow", frame );
        // Do not release the frame!                                                                   
        //If ESC key pressed, Key=0x10001B under OpenCV 0.9.7(linux version),                          
        //remove higher bits using AND operator                                                        
        if ( (cvWaitKey(10) & 255) == 27 ) break;
    }                                                                                                  
    // Release the capture device housekeeping                                                         
    cvReleaseCapture( &capture );
    cvDestroyWindow( "mywindow" );
    return 0;
}                                                                                                      
*/
