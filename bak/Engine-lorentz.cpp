
#include <algorithm>
#include <sys/time.h>

#include "const.h"
#include "Screen.h"
#include "SimulatedBodyModel.h"
#include "ControlledBodyModel.h"
#include "Engine.h"

using namespace std;
using namespace metric;

Engine::Engine( Screen *screen ) :
	Object(),
	_screen( screen )
{
	_currentTime = calcCurrentTime();
	_previousTime = _currentTime;
	_currentTimeDelta = 0.0;
}

Engine::~Engine()
{
}

bool Engine::registerBody( BodyModel *body )
{
	if ( std::find( _bodies.begin(), _bodies.end(), body ) != _bodies.end() ) {
		ERROR << "Tried to register body twice";
		return false;
	}
	// TODO: if body is deleted.. might want a signal in its destructor
	body->engine( this );
	_bodies.push_back( body );
	return true;
}

millisec_t Engine::calcCurrentTime() const
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return (millisec_t)tv.tv_sec * 1000000 + tv.tv_usec;
}

BodyModel* Engine::collisionOccured( const BodyModel *srcBody, const Vect2d &newSrcPos )
{
	std::list<BodyModel*>::iterator iBody = _bodies.begin();
	for( ; iBody != _bodies.end(); ++iBody )
	{
		BodyModel *dstBody = *iBody;

		if (dstBody == srcBody)
			continue;

		if (srcBody->collisionOccured( newSrcPos, dstBody ))
			return dstBody;
	}
	return NULL;
}

void Engine::step()
{
	preStep().emit();

	_currentTime = calcCurrentTime();
	_currentTimeDelta = double(_currentTime - _previousTime) / 1000000.0; // in seconds

	static double avgTimeDelta = 0.0;
	static unsigned int fpsCount = 1;

	avgTimeDelta += _currentTimeDelta / 100000;
	if (fpsCount++ % 100000 == 0) {
		USER << "FPS: " << (1.0 / avgTimeDelta) << endl;
		fpsCount = 1;
		avgTimeDelta = 0.0;
	}

	std::list<BodyModel*>::iterator iBody = _bodies.begin();
	for( ; iBody != _bodies.end(); ++iBody )
	{
		BodyModel* srcBody = *iBody;

		if (srcBody->simulated())
		{
			Vect2d newSrcPos = srcBody->stepPosition( currentTimeDelta() );
			Vect2d wallPos, wallNorm;

			//BodyModel* dstBody = collisionOccured(srcBody, newSrcPos);
			//if (dstBody) {
			if( wallCollisionOccured( srcBody, newSrcPos, wallPos, wallNorm ) ) {
				collision_puck_into_wall( srcBody, newSrcPos, wallPos, wallNorm);
			} else {
				srcBody->setPosition( newSrcPos );
			}
			srcBody->setAngle( srcBody->stepAngle(currentTimeDelta()) );
		}
		else
		{
			ControlledBodyModel *controlledBody = dynamic_cast<ControlledBodyModel*>( srcBody );
			if (controlledBody) 
			{
				Vect2d newSrcPos = controlledBody->getControlPosition();
				Vect2d dPos = newSrcPos - controlledBody->getPosition();
				controlledBody->setVelocity(
						controlledBody->getVelocity() * MALLET_RESPONSE_PERCENT +
						dPos * ((1.0 - MALLET_RESPONSE_PERCENT) / currentTimeDelta()) );

				BodyModel* dstBody = collisionOccured(srcBody, newSrcPos);
				if (dstBody) {
					collision_mallet_into_puck( srcBody, dstBody, newSrcPos );
					srcBody->setColor(255, 0, 0);
				} else {
					srcBody->setColor(255, 255, 255);
				}

				// update the dynamic position 
				controlledBody->BodyModel::setPosition( newSrcPos );
			}
		}
	}

	_previousTime = _currentTime;

	postStep().emit();
}

// ideal version which passes in coord of wall:
//bool wallCollisionOccured(const BodyModel *body, const Vect2d& newPos,
//		const Rect2d &wall, Vect2d& wallPos, Vect2d& wallNorm)
bool Engine::wallCollisionOccured(const BodyModel *body, const Vect2d& newPos,
						 		  Vect2d& wallPos, Vect2d& wallNorm) const
{
	Vect2d negPos = newPos - body->getRadius();
	Vect2d posPos = newPos + body->getRadius();
	
	if(negPos.x() <= 0.0) {
		// collision with left wall
		wallPos.x() = 0.0;
		wallPos.y() = newPos.y();
		wallNorm.y() = 0.0; // normal pointing right
		wallNorm.x() = 1.0;
		//wallTrans.y() = 1.0; // normal pointing right
		//wallTrans.x() = -1.0;
		return true;
	}

	if(negPos.y() <= 0.0) {
		// collision with top wall
		wallPos.x() = newPos.x();
		wallPos.y() = 0.0;
		wallNorm.y() = 1.0; // normal pointing down
		wallNorm.x() = 0.0;
		//wallTrans.y() = -1.0; // normal pointing down
		//wallTrans.x() = 1.0;
		return true;
	}

	if(posPos.x() >= _screen->world().x()) {
		// collision with right wall
		wallPos.x() = _screen->world().x();
		wallPos.y() = newPos.y();
		wallNorm.y() = 0.0; // normal pointing left
		wallNorm.x() = -1.0;
		//wallTrans.y() = 1.0; // normal pointing left
		//wallTrans.x() = -1.0;
		return true;
	}

	if(posPos.y() >= _screen->world().y()) {
		// collision with bottom wall
		wallPos.x() = newPos.x();
		wallPos.y() = _screen->world().y();
		wallNorm.y() = -1.0; // normal pointing up 
		wallNorm.x() = 0.0;
		//wallTrans.y() = -1.0; // normal pointing up 
		//wallTrans.x() = 1.0;
		return true;
	}

	return false;
}

void Engine::collision_mallet_into_puck(BodyModel *mallet, BodyModel *puck,
										const Vect2d& newMalletPos) const
{
	// why are these distances always about the same?  about 0.092
	// .. 9.2 cm?  
	double dist = mallet->getPosition().distance( puck->getPosition() );
	double dist2 = newMalletPos.distance( puck->getPosition() );

	USER << "Collision at distance: " << dist << "\tnewDist: " << dist2 << endl;

	if ( dist < 0.01 )
	{
		USER << "Collision less than 0.0!" << endl;
		// perform close-quarters "push-based" collision
	}
	else
	{
		// normal high-speed collision
	
		//Vect2d vel = mallet->getVelocity();
		Vect2d malletMomentum = mallet->getLinearMomentum();
		Vect2d puckMomentum = puck->getLinearMomentum();
		/*
		USER << "Collision: momentum mallet:" << malletMomentum*1000 << " puck:" << puckMomentum*1000 << endl;
		USER << "Collision: velocity mallet:" << mallet->getVelocity() << " puck:" << puck->getVelocity() << endl;
		USER << "Collision: mass mallet:" << mallet->getMass() << " puck:" << puck->getMass() << endl;
		*/

		//Vect2d pushDir = (puck->getPosition() - newMalletPos).normalize();
		Vect2d pushDir = (newMalletPos - puck->getPosition()).normalize();
		/*
		USER << "Collision: newMalletPos:" << newMalletPos << endl;
		USER << "Collision: PuckPos:" << puck->getPosition() << endl;
		USER << "Collision: pushDir:" << pushDir << endl;
		*/
		Vect2d collisionPos = puck->getPosition() + pushDir * puck->getRadius();
		Vect2d relativeMomentum = malletMomentum - puckMomentum;
		Vect2d pushMomentum = pushDir * relativeMomentum.dot( pushDir );
		puckMomentum += pushMomentum * 1.8; // my physics book says this should be by 'times 2.0'

		Vect2d linearSpinDir = pushDir.cross(); // orthogonal vector to pushDir.
		double angularSpinDir = linearSpinDir.crossZ( pushDir ); // postive or negative spin
		double spinMomentum = relativeMomentum.dot( linearSpinDir );
		if( angularSpinDir < 0.0 )
			spinMomentum = -spinMomentum;
		//Vect2d spinMomentum = spinDir * spinMagnitude;

		puck->setAngularMomentum( puck->getAngularMomentum() + spinMomentum );

		// O --> o 
		// P = mv -> v = P / m
		Vect2d puckVel = puckMomentum / puck->getMass();

		//USER << "Collision: " << puckVel << endl;
		puck->setVelocity( puckVel );
		//puck->setVelocity( puck->getVelocity() + vel );
		
		// These should be perpendicular:
		//g_impulses.push_back( Impulse(collisionPos, pushMomentum) );
		//g_impulses.push_back( Impulse(collisionPos, linearSpinDir * spinMomentum) );
		//g_lastCollision->setPosition( collisionPos );

		// Check if puck will still be in collision with mallet.
		Vect2d newPuckPos = puck->stepPosition( currentTimeDelta() );
		if(puck->collisionOccured( newPuckPos, mallet )) {
			double overlap = (puck->getRadius() + mallet->getRadius()) - 
							  newPuckPos.distance( mallet->getPosition() );
			assert( overlap > 0.0 );
			USER << " SUPER Collision! " << overlap << endl;

		//	if( overlap > puck->getRadius() / 4.0 ) {
		//		puck->setVelocity( puckVel + overlap/(8*currentTimeDelta()) );
		//	} else {
				puck->setPosition( puck->getPosition() + -pushDir * overlap );
		//	}
		}
	}
}

/*
void Engine::collision_puck_into_wall(BodyModel *puck, const Vect2d& newPos,
							  const Vect2d &wallPos,const Vect2d& wallNorm) const
{
	Vect2d wallDir = wallNorm.cross().normalize(); // dir along the wall
	Vect2d puckVel = puck->getVelocity();
	double puckAngVel = puck->getAngularVelocity();

	// direction of the resisting spin 
	//double spinResistance = puckVel.crossZ( wallNorm ) > 0.0 ? 1.0 : -1.0;

	// puck velocity in direction of wall
	//double wallVel = puckVel.dot( wallDir );

	// relative linear velocity of puck spin wrt. the wall
	//double wallLinearVel = puckAngVel * puck->getRadius() * spinResistance;

	Vect2d wallLinearVel = wallDir * puckAngVel * puck->getRadius();

	//double newWallVel = wallVel + wallLinearVel;

	// Ideal perfect reflection of puck without taking into account 
	// rotational resistance with wall.
	Vect2d idealVel = puckVel.reflect( wallNorm );

	//puck->setAngularVelocity( -puckAngVel + spinResistance );
	puck->setAngularVelocity( -puckAngVel );

	//USER << "Wall Collision: pos" << wallPos << " norm" << wallNorm << endl;
	//puck->setVelocity( idealVel );
	puck->setVelocity( idealVel + wallLinearVel );
}
*/

/* This is the second attempt.  It uses only transfers of velocities, 
 * incorrectly even.  No forces/torque is used.  Conservation of energy 
 * isn't maintained as the puck will gradually increase its linear velocity.
 *
void Engine::collision_puck_into_wall(BodyModel *puck, const Vect2d& newPos,
							  const Vect2d &wallPos,const Vect2d& wallNorm) const
{
	//
	// CLOCKWISE IS POSITIVE SPIN.
	// Vect2d::cross is set up in this handedness
	//
	// Transfer of energy from angular/linear momentums depends 
	// on two factors:
	//  1. relative linear velocity of edge of puck wrt. wall.
	//  2. duration of impact. (depending on angle of attack)
	//
	Vect2d puckVel = puck->getVelocity();
	double puckAngVel = puck->getAngularVelocity();

	double kineticEnergyBefore = puck->getKineticEnergy();
	double linearEnergyBefore = puck->getLinearKineticEnergy();
	double angularEnergyBefore = puck->getAngularKineticEnergy();
	//Vect2d linearMomentumBefore = puck->getLinearMomentum();
	//double angularMomentumBefore = puck->getAngularMomentum();
	//double momentumBefore = linearMomentumBefore.magnitude() + angularMomentumBefore;

	DUMP << "puckVel: " << puckVel << std::endl;
	DUMP << "puckAngVel: " << puckAngVel << std::endl;

	Vect2d towardWall = -wallNorm;
	Vect2d alongWall = wallNorm.cross().normalize(); // dir along the wall

	DUMP << "towardWall: " << towardWall << std::endl;
	DUMP << "alongWall: " << alongWall << std::endl;

	// this is the linear velocity from spin at impact along wall
	Vect2d spinAlongWall = alongWall * puckAngVel; // * puck->getRadius();

	DUMP << "spinAlongWall: " << spinAlongWall << std::endl;

	// This is the one dimensional velocity along the wall.
	double fromLinearVel = puckVel.dot( alongWall );
	double fromAngularVel = spinAlongWall.dot( alongWall );

	DUMP << "fromLinearVel: " << fromLinearVel << std::endl;
	DUMP << "fromAngularVel: " << fromAngularVel << std::endl;

	// this is the 1D speed at which the puck material hits 
	// the wall material.  Reactions are from resistance and 
	// restitution (bounce).
	double hitVel = fromLinearVel + fromAngularVel;
	double percentVelLost = 0.25; // 10% of angular vel is transferred to linear
	double degrade = 0.5;

	// rotation reaction opposite from spin direction
	double newVelOfAngular = -hitVel * (1.0 - percentVelLost) * degrade;
	//double newVelOfLinear = hitVel * percentVelLost * degrade;
	double newAngularVel = newVelOfAngular; // / puck->getRadius();

	DUMP << "puckAngVel: " << puckAngVel << std::endl;
	DUMP << "newAngularVel: " << newAngularVel << std::endl;
	DUMP << "diffAngVel: " << (newAngularVel - puckAngVel) << std::endl;

	//
	// Use lorentz transformation to determine new velocity vector 
	// for dimension along wall while keeping 'y' constant:
	//   x = sqrt( v*v - y*y )
	// This is to essentially 'extend' the velocity vector to be of 
	// length 'v' while keeping 'y' constant.
	// TODO: generalize to arbitrary wall normals.
	//
	double puckVelMag = puckVel.magnitude() + (newAngularVel - puckAngVel);
	double puckVelY = puckVel.dot( wallNorm ); // vel towards wall
	double puckVelX = puckVelMag * puckVelMag - puckVelY * puckVelY;

	if( puckVelX >= 0.0 ) {
		puckVelX = sqrt( puckVelX ) * (puckVel.dot( alongWall ) >= 0.0 ? 1.0 : -1.0);
	} else {
		puckVelX = puckVel.dot( alongWall );
	}

	//Vect2d newPuckVel = wallNorm * puckVelY + alongWall * puckVelX;

	DUMP << "puckVelMag: " << puckVelMag << std::endl;
	DUMP << "puckVelY: " << puckVelY << std::endl;
	DUMP << "puckVelX: " << puckVelX << std::endl;

	Vect2d newPuckVel;
	Vect2d transform;

	if (std::abs(puckAngVel) > 0.0) {
		double ratio = std::abs( puckVelX / puckVel.dot( alongWall ) );
		transform = (alongWall.abs() * ratio) + -wallNorm.abs();
	} else {
		transform = alongWall.abs() + -wallNorm.abs();
	}

	DUMP << "transform: " << transform << std::endl;
	newPuckVel = puckVel * transform;


	//// linear velocity towards wall is preserved, though mirrored)
	//Vect2d puckVelConst = wallNorm * puckVel.dot( wallNorm );
	//Vect2d puckVelVariable = puckVel - puckVelConst;

	//DUMP << "puckVelConst: " << puckVelConst << std::endl;
	//DUMP << "puckVelVariable: " << puckVelVariable << std::endl;

	//Vect2d newVelAlongWall = puckVelVariable + (alongWall * newVelOfLinear);
	//Vect2d newPuckVel = puckVelConst + newVelAlongWall;
	//DUMP << "newVelAlongWall: " << newVelAlongWall << std::endl;

	DUMP << "newPuckVel: " << newPuckVel << std::endl;

//	assert( (puckVelVariable - puckVelAlongWall).magnitude() <= 0.000001 );

	// TODO: took this out with addition of linearTransform
	//newPuckVel = newPuckVel.reflect( wallNorm );
	//
	// direction of the resisting spin 
	//double spinResistance = puckVel.crossZ( wallNorm ) > 0.0 ? 1.0 : -1.0;

	puck->setAngularVelocity( newAngularVel );

	//DUMP << "Wall Collision: pos" << wallPos << " norm" << wallNorm << endl;
	//puck->setVelocity( idealVel );
	puck->setVelocity( newPuckVel );

	//Vect2d linearMomentumAfter = puck->getLinearMomentum();
	//double angularMomentumAfter = puck->getAngularMomentum();
	//double momentumAfter = linearMomentumAfter.magnitude() + angularMomentumAfter;
	//if( std::abs(momentumAfter - momentumBefore) > 0.0 ) {
//		DUMP << "Tried to defy conservation of momentum: " << std::endl;
//		DUMP << "momentumBefore: " << momentumBefore << " " << linearMomentumBefore << 
//			"=" << linearMomentumBefore.magnitude() << " " << angularMomentumBefore << endl;
//		DUMP << "momentumAfter: " << momentumAfter << " " << linearMomentumAfter <<
//			"=" << linearMomentumAfter.magnitude() << " " << angularMomentumAfter << endl;
//		assert(false);
//	}
//
	double kineticEnergyAfter = puck->getKineticEnergy();
	double linearEnergyAfter = puck->getLinearKineticEnergy();
	double angularEnergyAfter = puck->getAngularKineticEnergy();

	DUMP << "kineticEnergyBefore: " << kineticEnergyBefore << " " << linearEnergyBefore << 
		" " << angularEnergyBefore << endl;
//		"=" << linearMomentumBefore.magnitude() << " " << angularMomentumBefore << endl;
	DUMP << "kineticEnergyAfter: " << kineticEnergyAfter << " " << linearEnergyAfter <<
		" " << angularEnergyAfter << endl;
//		"=" << linearMomentumAfter.magnitude() << " " << angularMomentumAfter << endl;

	//if( std::abs(kineticEnergyAfter - kineticEnergyBefore) > 0.0 ) {
	if( kineticEnergyAfter > kineticEnergyBefore ) {
		DUMP << "Tried to defy conservation of energy." << std::endl;
	//	assert(false);
	}

	DUMP << "newPuckVelRefl: " << puck->getVelocity() << std::endl;
	DUMP << "newPuckAngVel: " << puck->getAngularVelocity() << std::endl;

	exit(0);
}
 */


/*
 * This is the third attempt.  I'm going to try to use torque and frictional forces 
 * over a small time delta to make this work.
 */
void Engine::collision_puck_into_wall(BodyModel *puck, const Vect2d& newPos,
							  const Vect2d &wallPos,const Vect2d& wallNorm) const
{
	//
	// CLOCKWISE IS POSITIVE SPIN.
	// Vect2d::cross is set up in this handedness
	//
	Vect2d puckVel = puck->getVelocity();
	double puckAngVel = puck->getAngularVelocity();

	// 
	// Rolling: pg307, pg1295 for answers
	//
	// Assumptions:
	// 	- no slipping on contact
	// equations:
	// 	- linear_acc_at_center_of_mass = angular_acceleration * radius
	// 	- torque = r * F
	// 	- torque = I * angular_acceleration
	// forces:
	// 	- Fs = static frictional force opposite direction of rotation on wall.
	// 		- it is the wall's opposition to the puck along wall.
	// 		- radius * Fs = I * angular_acceleration
	// 	- Fn = normal force perpendicular and away from the wall at the point of collision.
	// 		- the wall's opposition to the puck directed away from the wall.
	// 	- Fl = linear force of puck's collision into wall.  
	// 		- I'm not sure how to convert momentum into a force here.
	//
	// forces along wall:
	// 	- (Fs - Fl_along_wall) = M * acc_along_wall
	// 	- Fs == -I * acc_along_wall / r^2
	// forces against wall:
	// 	- 
	Vect2d newPuckVel = puckVel;
	double newAngVel = puckAngVel;

	puck->setAngularVelocity( newAngVel );
	puck->setVelocity( newPuckVel );
}


